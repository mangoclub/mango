package com.mango.exception;


public class LockFailException extends RuntimeException {

    public LockFailException(String message) {
        super(message);
    }

    public LockFailException(String message, Throwable cause) {
        super(message, cause);
    }
}
