
package com.mango.filter;

import com.mango.context.UserContext;
import com.mango.enums.DailyRecordTypeEnum;
import org.apache.commons.lang.StringUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class SessionFilter implements Filter {


	private String excludedPages;
	private String[] excludedPageArray;
	private String adminPages;
	private String[] adminPagesArray;

	public void init(FilterConfig filterConfig) throws ServletException {
		excludedPages = filterConfig.getInitParameter("excludedPages");
		if (StringUtils.isNotEmpty(excludedPages)) {
			excludedPageArray = excludedPages.split(",");
		}
		adminPages = filterConfig.getInitParameter("adminPages");
		if (StringUtils.isNotEmpty(adminPages)) {
			adminPagesArray = adminPages.split(",");
		}

		return;
	}

	public void doFilter(ServletRequest servletRequest,
			ServletResponse servletResponse, FilterChain chain)
			throws IOException, ServletException {

		boolean isExcludedPage = false;
		for (String page : excludedPageArray) {//判断是否在过滤url之外
			if(((HttpServletRequest) servletRequest).getServletPath().equals(page)){
				isExcludedPage = true;
				break;
			}
		}
		boolean isAdminPage = false;
		for (String page : adminPagesArray) {//判断是否在过滤url之外
			if(((HttpServletRequest) servletRequest).getServletPath().contains(page)){
				isAdminPage = true;
				break;
			}
		}
		HttpServletRequest request = (HttpServletRequest) servletRequest;
		HttpServletResponse response = (HttpServletResponse) servletResponse;

		response.setHeader("Pragma","No-cache");
		response.setHeader("Cache-Control","no-cache");
		response.setDateHeader("Expires",   0);

		if (isExcludedPage) {//在过滤url之外
			chain.doFilter(request, response);
			return;
		}


		String userInfo = (String) request.getSession().getAttribute("userInfo");
		if(StringUtils.isEmpty(userInfo) && !request.getServletPath().contains("/api/")){
			response.sendRedirect(getBasePath(request) + "/");
			return;
		}


		if (isAdminPage) {
			UserContext userContext = (UserContext)request.getSession().getAttribute("userContext");

			if (userContext == null || userContext.getUserDto() == null ||userContext.getUserDto().getExtend1() == null || !userContext.getUserDto().getExtend1().equals("admin")) {
				response.sendRedirect(getBasePath(request) + "/dashboard");
				return;
			}

		}

		chain.doFilter(request, response);
	}

	private String getBasePath(HttpServletRequest request) {
		String path = request.getContextPath();
		String basePath = request.getScheme() + "://"
				+ request.getServerName() + ":" + request.getServerPort()
				+ path;
		return basePath;
	}

	public void destroy() {

		return;
	}

}
