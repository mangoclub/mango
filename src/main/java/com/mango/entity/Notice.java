package com.mango.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;

import java.math.BigDecimal;
import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Notice extends BasicEntity {
	private String title;//名称
	private String content;//内容
	private Date startDate;//开始时间
	private Date finishDate;//结束时间
	private Integer status;//状态

	private String statusName;//状态描述

}
