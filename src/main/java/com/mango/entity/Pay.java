package com.mango.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;

import java.math.BigDecimal;
import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Pay extends BasicEntity {
	private Integer extendMonth;
	private BigDecimal money;
	private String extendDate;
	private String expiredDate;
	private Integer status;
	private String statusName;
	private String accountName;
}
