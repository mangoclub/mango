package com.mango.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User extends BasicEntity {
	private Integer shopId;
	private String accountName;
	private String password;
	private String newPassword;
	private String ip;
	private String phone;
	private String userAgent;
}
