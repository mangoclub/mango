package com.mango.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Vendor extends BasicEntity {
	private String name;
	private String phone;
	private String linkman;
	private String remarks;
}
