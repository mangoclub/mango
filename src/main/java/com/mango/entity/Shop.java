package com.mango.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Shop extends BasicEntity {
	private String shopName;
	private String accountName;
	private String address;
	private String linkman;
	private String phone;
	private String password;
	private String ip;
	private String userAgent;//浏览器

}
