package com.mango.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;
import org.omg.PortableInterceptor.INACTIVE;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DistributedLock extends BasicEntity {
    private String key;
    private Integer type;
    private Date expiredTime;
}