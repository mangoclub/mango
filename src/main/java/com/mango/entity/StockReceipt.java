package com.mango.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;

import java.math.BigDecimal;
import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StockReceipt extends BasicEntity {
	private Integer fromShopId;//入库门店id
	private Integer toShopId;//出库门店id
	private String serial;//单据编号
	private Date receiptDate;//单据日期
	private Date stockChangeTime;//出入库时间
	private Integer receiptType;//单据类型
	private Integer supplierId;//供应商id
	private Integer clientId;//客户id
	private Integer status;//单据状态
	private String remarks;//备注
	private Integer totalCount;//总数
	private BigDecimal totalMoney;//总额
	private String hasPay;

	private String supplierName;
	private String clientName;
}
