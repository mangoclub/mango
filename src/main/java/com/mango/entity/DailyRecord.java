package com.mango.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DailyRecord extends BasicEntity {
	private String accountName;//账号名
	private Integer type;//日志类型
	private String direction;//说明

	private String shopName;
}
