package com.mango.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;
import org.omg.PortableInterceptor.INACTIVE;

import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class InventoryChangeFlow extends BasicEntity {

	private Integer changeShopId;//出入库门店id
	private Integer type;//类型
	private Integer receiptId;//单据id
	private Integer productId;//产品id
	private int inCount;//入库数量
	private BigDecimal inPrice;//入库单价
	private BigDecimal inMoney;//入库金额
	private int outCount;//出库数量
	private BigDecimal outPrice;//出库单价
	private BigDecimal outMoney;//出库金额
	private BigDecimal gainMoney;//盈利金额
	private int beforeInventory;//变化前库存
	private int afterInventory;//变化后库存
	private	BigDecimal beforeCostPrice;//变化前成本价
	private BigDecimal afterCostPrice;//变化后成本价
	private BigDecimal totalCost;//产品总成本
	//以下为显示字段
	private String shopName;//门店名称
	private String changeShopName;//出入库门店名称
	private String productName;//产品名称
	private String productCode;//产品编码
	private String productUnit;//产品单位
	private String serial;//单据号
	private String stockChangeTime;

}
