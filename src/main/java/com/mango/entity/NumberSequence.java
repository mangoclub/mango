package com.mango.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class NumberSequence extends BasicEntity {
    private Integer shopId;
    private Integer type;
    private String value;
}