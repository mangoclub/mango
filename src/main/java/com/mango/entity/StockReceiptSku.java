package com.mango.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;

import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StockReceiptSku extends BasicEntity {
	private Integer receiptId;// 单据 ID
	private Integer productId;//产品id
	private int count;//数量
	private BigDecimal price;//价格
	private BigDecimal money;//金额
	private String remarks;//备注

	private String productName;
	private String productCode;
	private String unit;

	private int stockInventory;

}
