package com.mango.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;

import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Product extends BasicEntity {
	private Integer shopId;//门店id
	private String name;//名称
	private String code;//编码
	private String  imageUrl;//图片url
	private String  imagePath;//图片path
	private String  unit;//单位
	private BigDecimal retailPrice;//零售价
	private String beforeDate;//保质期
	private String remarks;//备注

	private int stockInventory;
}
