package com.mango.aop;

import com.google.gson.Gson;
import com.mango.context.ContextContainer;
import com.mango.context.UserContext;
import com.mango.dto.ResultMap;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.Map;
@Component
@Aspect
public class CheckSession  {

    static class TextView implements View {
        private ResultMap msg;

        public TextView(ResultMap msg) {
            this.msg = msg;
        }

        @Override
        public String getContentType() {
            return "application/json";
        }

        @Override
        public void render(Map<String, ?> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
            response.setContentType(getContentType());
            response.setStatus(500);
            PrintWriter writer = response.getWriter();
            writer.print(new Gson().toJson(this.msg));
            writer.flush();
        }

    }

    public ModelAndView check() {
        UserContext userContext = ContextContainer.getUserContext();

        return new ModelAndView(new TextView(ResultMap.builder().code(500).msg("由于您长时间未操作,请刷新页面,重新登入").build()));

    }

}