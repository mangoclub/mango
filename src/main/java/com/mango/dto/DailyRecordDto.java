package com.mango.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;

import java.io.Serializable;
import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DailyRecordDto extends BasicDto implements Serializable {
	private String accountName;//账号名
	private Integer type;//日志类型
	private String direction;//说明
	private String shopName;

	public static void convert(DailyRecordDto dto) {}
}
