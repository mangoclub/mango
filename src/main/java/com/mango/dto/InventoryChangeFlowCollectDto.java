package com.mango.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class InventoryChangeFlowCollectDto implements Serializable {
	private Date date;
	private List<InventoryChangeFlowDataDto> data;
}
