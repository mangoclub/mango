package com.mango.dto;


import com.mango.enums.NumberSequenceTypeEnum;
import com.mango.enums.PayEnum;
import com.mango.enums.StockReceiptStatusEnum;
import com.mango.utils.Assert;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StockReceiptDto extends BasicDto implements Serializable {

	private Integer fromShopId;//入库门店id
	private Integer toShopId;//出库门店id
	private String serial;//单据编号
	private Date receiptDate;//单据日期
	private Date stockChangeTime;//出入库时间
	private Integer receiptType;//单据类型
	private Integer supplierId;//供应商id
	private Integer clientId;//客户id
	private Integer status;//单据状态
	private String remarks;//备注
	private Integer totalCount;//总数
	private BigDecimal totalMoney;//总额
	private String hasPay;

	private String typeName;
	private String statusName;
	private String supplierName;
	private String clientName;
	private String hasPayName;

	List<StockReceiptSkuDto> list;

	public static void checkAndTransfer(StockReceiptDto dto) {
	}

	public static void convert(StockReceiptDto dto) {
		dto.setTypeName(NumberSequenceTypeEnum.getDescByCode(dto.getReceiptType()));
		dto.setStatusName(StockReceiptStatusEnum.getDescByTypeAndCode(dto.getReceiptType(), dto.getStatus()));
		dto.setHasPayName(PayEnum.getDescByCode(dto.getHasPay()));
	}



}
