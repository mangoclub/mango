package com.mango.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class InventoryDto extends BasicDto implements Serializable {

	private Integer productId;//产品id
	private int stockInventory;//实际库存
	private int stockAvailable;//可用库存
	private int stockFrozen;//冻结库存
	private BigDecimal costPrice = BigDecimal.ZERO;//成本价
	private BigDecimal totalCost = BigDecimal.ZERO;//产品总成本
	//以下为显示字段
	private String shopName;//门店名称
	private String productName;//产品名称
	private String productCode;//产品编码
	private String productUnit;//产品单位


	public static void checkAndTransfer(InventoryDto dto) {

	}

	public static void convert(InventoryDto dto) {

	}

}
