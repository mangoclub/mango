package com.mango.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;

import java.io.Serializable;
import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class VendorDto extends BasicDto implements Serializable {
	private String phone;
	private String linkman;
	private String remarks;


	public static void checkAndTransfer(VendorDto vendorDto) {

	}

	public static void convert(VendorDto vendorDto) {

	}

}
