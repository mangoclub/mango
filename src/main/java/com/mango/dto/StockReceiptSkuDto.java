package com.mango.dto;


import com.mango.utils.Assert;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StockReceiptSkuDto extends BasicDto implements Serializable {

	private Integer receiptId;//单据id
	private Integer productId;//产品id
	private Integer count;//数量
	private BigDecimal price = BigDecimal.ZERO;//价格
	private BigDecimal money = BigDecimal.ZERO;//金额
	private String remarks;//备注

	private String productName;
	private String productCode;
	private String unit;

	private int stockInventory;


	public static void checkAndTransfer(StockReceiptSkuDto dto) {
		Assert.assertFalse(dto.getProductId() == null, "请选择产品！");
		Assert.assertFalse(dto.getCount() == null && dto.getCount() > 0, "数量必须大于0！");
		Assert.assertFalse(dto.getPrice() == null, "请选择价格！");
	}

	public static void convert(StockReceiptSkuDto dto) {

	}

}
