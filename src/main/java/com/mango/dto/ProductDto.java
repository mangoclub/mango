package com.mango.dto;


import com.mango.utils.Assert;
import com.mysql.jdbc.StringUtils;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductDto extends BasicDto implements Serializable {

	private String code;//编码
	private String  imageUrl;//图片url
	private String  imagePath;//图片path
	private String  unit;//单位
	private BigDecimal retailPrice = BigDecimal.ZERO;//零售价
	private String beforeDate;//保质期
	private String remarks;//备注

	private int stockInventory;


	public static void checkAndTransfer(ProductDto dto) {
		Assert.assertFalse(StringUtils.isNullOrEmpty(dto.getName()), "请填写产品名称！");
		Assert.assertFalse(StringUtils.isNullOrEmpty(dto.getName().trim()), "请填写机构名称！");
		Assert.assertFalse(StringUtils.isNullOrEmpty(dto.getCode()), "请填写产品编码！");
		Assert.assertFalse(StringUtils.isNullOrEmpty(dto.getCode().trim()), "请填写产品编码！");
		Assert.assertFalse(StringUtils.isNullOrEmpty(dto.getUnit()), "请填写产品单位！");
		Assert.assertFalse(StringUtils.isNullOrEmpty(dto.getUnit().trim()), "请填写产品单位！");

		Assert.assertFalse(dto.getRetailPrice() == null , "请填写零售价！");
		Assert.assertFalse(dto.getRetailPrice().compareTo(BigDecimal.ZERO) == -1 , "零售价>=0！");
	}

	public static void convert(ProductDto dto) {

	}

}
