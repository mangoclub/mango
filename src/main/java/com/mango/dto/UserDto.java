package com.mango.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDto extends BasicDto implements Serializable {
	private String accountName;
	private String password;
	private String newPassword;
	private String phone;


	public static void checkAndTransfer(UserDto dto) {

	}

	public static void convert(UserDto dto) {

	}
}
