package com.mango.dto;


import com.mango.enums.NoticeStatusEnum;
import com.mango.utils.Assert;
import com.mysql.jdbc.StringUtils;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;

import java.io.Serializable;
import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class NoticeDto extends BasicDto implements Serializable {
	private String title;//名称
	private String content;//内容
	private Date startDate;//开始时间
	private Date finishDate;//结束时间
	private Integer status;//状态

	private String statusName;//状态描述


	public static void checkAndTransfer(NoticeDto dto) {
		Assert.assertFalse(StringUtils.isNullOrEmpty(dto.getTitle()), "请填写标题！");
		Assert.assertFalse(dto.getTitle().length()>280, "标题太长！");
		Assert.assertFalse(StringUtils.isNullOrEmpty(dto.getContent()), "请填写内容！");
		Assert.assertFalse(dto.getContent().length()>480, "内容太长！");
		Assert.assertFalse(dto.getStartDate() == null, "请填写开始发布时间！");
		Assert.assertFalse(dto.getFinishDate() == null, "请填写结束发布时间！");
	}

	public static void convert(NoticeDto dto) {
		dto.setStatusName(NoticeStatusEnum.getDescByCode(dto.getStatus()));
	}
}
