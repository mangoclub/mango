package com.mango.dto;

import com.mango.context.ContextContainer;
import com.mango.context.UserContext;
import com.mysql.jdbc.StringUtils;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;
import org.omg.CORBA.INTERNAL;

import java.io.Serializable;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class QueryCondition implements Serializable {
    private Integer supplierId;
    private Integer clientId;
    private String keyword;
    private Integer shopId;
    private String type;
    private Integer receiptType;
    private int start;
    private int limit;
    private String current;
    private String pageSize;

    private Integer receiptId;

    public static void checkAndTransfer(QueryCondition queryCondition) {
        UserContext userContext = ContextContainer.getUserContext();

        String current = queryCondition.getCurrent();
        String pageSize = queryCondition.getPageSize();

        int start = 0;
        int limit = 10;

        if(!StringUtils.isNullOrEmpty(current) && !StringUtils.isNullOrEmpty(pageSize)) {
            limit = Integer.parseInt(pageSize);
            start = (Integer.parseInt(current) - 1) * limit;
        }

        queryCondition.setStart(start);
        queryCondition.setLimit(limit);
        queryCondition.setShopId(userContext.getShopId());
    }
}
