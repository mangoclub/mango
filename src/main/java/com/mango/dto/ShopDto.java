package com.mango.dto;


import com.mango.utils.Assert;
import com.mysql.jdbc.StringUtils;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ShopDto extends BasicDto implements Serializable {
	private String shopName;
	private String accountName;
	private String address;
	private String linkman;
	private String phone;
	private String password;
	private String newPassword;


	public static void checkAndTransfer(ShopDto shopDto) {
		Assert.assertFalse(StringUtils.isNullOrEmpty(shopDto.getShopName()), "请填写机构名称！");
		Assert.assertFalse(StringUtils.isNullOrEmpty(shopDto.getShopName().trim()), "请填写机构名称！");
		Assert.assertFalse(StringUtils.isNullOrEmpty(shopDto.getAccountName()), "请填写帐号！");
		Assert.assertFalse(StringUtils.isNullOrEmpty(shopDto.getAccountName().trim()), "请填写帐号！");
		Assert.assertFalse(StringUtils.isNullOrEmpty(shopDto.getIp()), "ip信息错误！");

		Assert.assertFalse(shopDto.getShopName().trim().length() < 2, "机构名称最少 2 位！");
		Assert.assertFalse(shopDto.getAccountName().trim().length() < 3, "帐号最少 3 位！");

		shopDto.setAccountName(shopDto.getAccountName().trim());
	}

	public static void convert(ShopDto shopDto) {

	}
}
