package com.mango.dto;

import com.mango.utils.Assert;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;
import org.springframework.util.CollectionUtils;

import java.io.Serializable;
import java.util.List;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class InventoryChangeDto implements Serializable {

    private Integer type;
    private Integer productId;
    private Integer shopId;
    private Integer fromShopId;
    private Integer toShopId;
    private Integer receiptId;

    List<StockReceiptSkuDto> skuDtoList;

    public static void checkAndTransfer(InventoryChangeDto dto) {
        Assert.assertFalse(dto.getShopId() == null, "门店为空！");
        Assert.assertFalse(dto.getType() == null, "类型为空！");
        Assert.assertFalse(CollectionUtils.isEmpty(dto.getSkuDtoList()), "产品列表为空！");
    }

    public static void convert(InventoryChangeDto dto) {

    }
}
