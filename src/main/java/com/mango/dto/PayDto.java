package com.mango.dto;


import com.mango.enums.PayStatusEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PayDto extends BasicDto implements Serializable {
	private Integer extendMonth;
	private BigDecimal money;
	private String extendDate;//延长到日期
	private String expiredDate;//到期
	private Integer status;
	private String statusName;
	private String accountName;


	public static void checkAndTransfer(PayDto dto) {

	}

	public static void convert(PayDto dto) {
		dto.setStatusName(PayStatusEnum.getDescByCode(dto.getStatus()));
	}
}
