package com.mango.dto;


import com.mango.enums.ClientTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;

import java.io.Serializable;
import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClientDto extends BasicDto implements Serializable {
	private String type;
	private String typeName;
	private String phone;
	private String remarks;

	public static void checkAndTransfer(ClientDto clientDto) {
		clientDto.setType(clientDto.getType() == null ? "1" : clientDto.getType());
	}
	public static void convert(ClientDto clientDto) {
		clientDto.setTypeName(clientDto.getType() == null ? "1" : ClientTypeEnum.getDescByCode(clientDto.getType()));
	}
}
