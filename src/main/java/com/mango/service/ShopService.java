package com.mango.service;


import com.mango.context.UserContext;
import com.mango.dto.QueryCondition;
import com.mango.dto.ShopDto;
import com.mango.dto.ShopWebDto;

import javax.servlet.http.HttpSession;
import java.util.List;

public interface ShopService {

    int create(ShopDto shopDto);

    int update(ShopDto dto);

    int extend(ShopDto dto);

    ShopDto login(ShopDto shopDto, HttpSession httpSession, UserContext userContext);

    boolean logout(HttpSession httpSession);

    boolean changePassword(ShopDto shopDto, HttpSession httpSession);

    boolean resetPassword(ShopDto shopDto, HttpSession httpSession);

    ShopDto userInfo(HttpSession httpSession);

    ShopDto findById(Integer id);

    List<ShopDto> queryByConditionWithAnd(ShopDto shopDto);

    void enrichUserInfo(Integer shopId, HttpSession httpSession, UserContext userContext);

    List<ShopDto> queryByCondition(QueryCondition queryCondition);

    int queryCountByCondition(QueryCondition queryCondition);

    ShopWebDto query(QueryCondition queryCondition);

}