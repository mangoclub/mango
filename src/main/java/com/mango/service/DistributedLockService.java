package com.mango.service;


public interface DistributedLockService {

    Boolean lock(Integer lockType, Integer group, String key);

    Boolean unLock(Integer lockType, Integer group, String key);

}
