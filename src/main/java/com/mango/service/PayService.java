package com.mango.service;


import com.mango.dto.QueryCondition;
import com.mango.dto.PayDto;
import com.mango.dto.PayWebDto;

import java.util.List;

public interface PayService {

    int create(PayDto dto);

    int update(PayDto dto);

    int cofirm (PayDto dto);

    int delete (PayDto dto);

    PayDto findById(Integer id);

    List<PayDto> queryByCondition(QueryCondition queryCondition);

    int queryCountByCondition(QueryCondition queryCondition);

    PayWebDto query(QueryCondition queryCondition);

}