package com.mango.service;


import com.mango.dto.InventoryChangeFlowCollectDto;
import com.mango.dto.InventoryChangeFlowDto;
import com.mango.dto.InventoryChangeFlowWebDto;
import com.mango.dto.QueryCondition;

import java.util.List;


public interface InventoryChangeFlowService {

    int create(InventoryChangeFlowDto dto);

    int update(InventoryChangeFlowDto dto);

    InventoryChangeFlowDto findById(Integer id);

    List<InventoryChangeFlowDto> queryByCondition(QueryCondition queryCondition);

    int queryCountByCondition(QueryCondition queryCondition);

    InventoryChangeFlowWebDto query(QueryCondition queryCondition);

    List<InventoryChangeFlowCollectDto> queryCollect(QueryCondition queryCondition);

}