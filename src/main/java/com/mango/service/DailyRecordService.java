package com.mango.service;


import com.mango.dto.DailyRecordDto;
import com.mango.dto.DailyRecordWebDto;
import com.mango.dto.QueryCondition;

import java.util.List;

public interface DailyRecordService {

    int create(DailyRecordDto dailyRecordDto);

    int create(Integer type, String direction);

    int create(String ip, Integer type, String direction);

    List<DailyRecordDto> queryByCondition(QueryCondition queryCondition);

    int queryCountByCondition(QueryCondition queryCondition);

    DailyRecordWebDto query(QueryCondition queryCondition);

}