package com.mango.service;


import com.mango.dto.QueryCondition;
import com.mango.dto.StockReceiptSkuDto;
import com.mango.dto.StockReceiptSkuWebDto;

import java.util.List;

public interface StockReceiptSkuService {

    int create(StockReceiptSkuDto dto);

    int update(StockReceiptSkuDto dto);

    boolean delete(List<Integer> ids);

    StockReceiptSkuDto findById(Integer id);

    List<StockReceiptSkuDto> queryByCondition(QueryCondition queryCondition);

    List<StockReceiptSkuDto> queryList(QueryCondition queryCondition);

    int queryCountByCondition(QueryCondition queryCondition);

    StockReceiptSkuWebDto query(QueryCondition queryCondition);

}