package com.mango.service;

import com.google.common.collect.Lists;
import com.mango.dto.BasicDto;
import com.mango.dto.LabelValueDto;
import java.util.List;


public abstract class AbstractBasicService<T extends BasicDto> {


    public List<LabelValueDto> generateLabelValueList(List<T> list) {

        List<LabelValueDto> labelValueDtos = Lists.newArrayList();
        list.stream().forEach(c->{
            LabelValueDto labelValueDto = LabelValueDto.builder().label(c.getName()).value(c.getId()).build();
            labelValueDtos.add(labelValueDto);
        });
        return labelValueDtos;

    }

}
