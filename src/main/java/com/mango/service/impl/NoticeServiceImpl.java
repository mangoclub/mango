package com.mango.service.impl;


import com.google.common.collect.Lists;
import com.mango.context.ContextContainer;
import com.mango.context.UserContext;
import com.mango.dao.NoticeDao;
import com.mango.dto.NoticeDto;
import com.mango.dto.NoticeWebDto;
import com.mango.dto.QueryCondition;
import com.mango.entity.Notice;
import com.mango.enums.DailyRecordTypeEnum;
import com.mango.enums.NoticeStatusEnum;
import com.mango.framework.BeanMappingService;
import com.mango.service.AbstractBasicService;
import com.mango.service.DailyRecordService;
import com.mango.service.InventoryService;
import com.mango.service.NoticeService;
import com.mango.utils.ApplicationException;
import com.mango.utils.Assert;
import com.mango.utils.EntityUtils;
import lombok.extern.log4j.Log4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Log4j
public class NoticeServiceImpl extends AbstractBasicService<NoticeDto> implements NoticeService {


    @Autowired
    NoticeDao dao;
    @Autowired
    private BeanMappingService beanMappingService;
    @Autowired
    private DailyRecordService dailyRecordService;

    @Override
    public int create(NoticeDto dto) {
        try {
            NoticeDto.checkAndTransfer(dto);
            Notice entity = beanMappingService.transform(dto, Notice.class);
            EntityUtils.init(entity);
            dao.create(entity);
            if(entity.getStatus().equals(NoticeStatusEnum.PUBLISH.getCode())) {
                dto.setId(entity.getId());
                publish(dto);
            }

            dailyRecordService.create(DailyRecordTypeEnum.CREATE_NOTICE_S.getCode(), DailyRecordTypeEnum.CREATE_NOTICE_S.getDesc());
            return entity.getId();
        }catch (Exception e) {
            dailyRecordService.create(DailyRecordTypeEnum.CREATE_NOTICE_E.getCode(), StringUtils.join(Lists.newArrayList(DailyRecordTypeEnum.CREATE_NOTICE_E.getDesc(), e.getMessage()), ","));
            throw e;
        }

    }

    @Override
    public int update(NoticeDto dto) {
        try {
            NoticeDto.checkAndTransfer(dto);
            Notice entity = beanMappingService.transform(dto, Notice.class);
            EntityUtils.update(entity);
            dao.update(entity);
            if(entity.getStatus().equals(NoticeStatusEnum.PUBLISH.getCode())) {
                dto.setId(entity.getId());
                publish(dto);
            }

            dailyRecordService.create(DailyRecordTypeEnum.UPDATE_NOTICE_S.getCode(), DailyRecordTypeEnum.UPDATE_NOTICE_S.getDesc());
            return entity.getId();
        }catch (Exception e) {
            dailyRecordService.create(DailyRecordTypeEnum.UPDATE_NOTICE_E.getCode(), StringUtils.join(Lists.newArrayList(DailyRecordTypeEnum.UPDATE_NOTICE_E.getDesc(), e.getMessage()), ","));
            throw e;
        }
    }



    @Override
    public List<NoticeDto> queryByCondition(QueryCondition queryCondition) {
        try {
            QueryCondition.checkAndTransfer(queryCondition);
            List<Notice> entitys = dao.queryByCondition(queryCondition);
            List<NoticeDto> dtos = Lists.newArrayList();
            if(!CollectionUtils.isEmpty(entitys)) {
                for(Notice entity : entitys) {
                    NoticeDto dto = beanMappingService.transform(entity, NoticeDto.class);
                    NoticeDto.convert(dto);
                    dtos.add(dto);
                }
            }

            return dtos;
        }catch (Exception e) {
            throw e;
        }
    }

    @Override
    public int queryCountByCondition(QueryCondition queryCondition) {
        try {
            QueryCondition.checkAndTransfer(queryCondition);
            int total = dao.queryCountByCondition(queryCondition);

            return total;
        }catch (Exception e) {
            throw e;
        }
    }

    @Override
    public NoticeWebDto query(QueryCondition queryCondition) {
        try {
            NoticeWebDto webDto = NoticeWebDto.builder()
                    .total(queryCountByCondition(queryCondition))
                    .list(queryByCondition(queryCondition))
                    .build();
            dailyRecordService.create(DailyRecordTypeEnum.QUERY_NOTICE_S.getCode(), DailyRecordTypeEnum.QUERY_NOTICE_S.getDesc());
            return webDto;
        } catch (Exception e) {
            dailyRecordService.create(DailyRecordTypeEnum.QUERY_NOTICE_E.getCode(), StringUtils.join(Lists.newArrayList(DailyRecordTypeEnum.QUERY_NOTICE_E.getDesc(), e.getMessage()), ","));
            throw e;
        }
    }

    @Override
    public NoticeDto findById(Integer id) {
        NoticeDto dto = beanMappingService.transform(dao.findById(id), NoticeDto.class);
        return dto;
    }

    @Override
    public int publish(NoticeDto dto) {
        try {
            dto.setStatus(NoticeStatusEnum.RELEASE.getCode());
            Notice entity = beanMappingService.transform(dto, Notice.class);
            EntityUtils.update(entity);
            dao.release(entity);
            entity.setStatus(NoticeStatusEnum.PUBLISH.getCode());
            dao.publish(entity);

            dailyRecordService.create(DailyRecordTypeEnum.PUBLISH_NOTICE_S.getCode(), DailyRecordTypeEnum.PUBLISH_NOTICE_S.getDesc());
            return entity.getId();
        }catch (Exception e) {
            dailyRecordService.create(DailyRecordTypeEnum.PUBLISH_NOTICE_E.getCode(), StringUtils.join(Lists.newArrayList(DailyRecordTypeEnum.PUBLISH_NOTICE_E.getDesc(), e.getMessage()), ","));
            throw e;
        }
    }

    @Override
    public int release(NoticeDto dto) {
        try {
            dto.setStatus(NoticeStatusEnum.RELEASE.getCode());
            Notice entity = beanMappingService.transform(dto, Notice.class);
            EntityUtils.update(entity);
            dao.publish(entity);

            dailyRecordService.create(DailyRecordTypeEnum.RELEASE_NOTICE_S.getCode(), DailyRecordTypeEnum.RELEASE_NOTICE_S.getDesc());
            return entity.getId();
        }catch (Exception e) {
            dailyRecordService.create(DailyRecordTypeEnum.RELEASE_NOTICE_E.getCode(), StringUtils.join(Lists.newArrayList(DailyRecordTypeEnum.RELEASE_NOTICE_E.getDesc(), e.getMessage()), ","));
            throw e;
        }
    }

    @Override
    public NoticeDto findByStatus(Integer status) {
        NoticeDto dto = beanMappingService.transform(dao.findByStatus(status), NoticeDto.class);
        if(dto != null && dto.getStartDate() != null && dto.getFinishDate() != null)  {
            if(dto.getStartDate().compareTo(new Date()) >0 || dto.getFinishDate().compareTo(new Date()) < 0) {
                setNullNotice(dto);
            }
        }else {
            dto = new NoticeDto();
            setNullNotice(dto);
        }
        return dto;
    }

    private void setNullNotice(NoticeDto dto) {
        dto.setTitle("");
        dto.setContent("");
        dto.setStatus(NoticeStatusEnum.RELEASE.getCode());
    }

}
