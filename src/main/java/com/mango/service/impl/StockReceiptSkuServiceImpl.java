package com.mango.service.impl;


import com.google.common.collect.Lists;
import com.mango.dao.StockReceiptSkuDao;
import com.mango.dto.*;
import com.mango.entity.StockReceiptSku;
import com.mango.enums.DailyRecordTypeEnum;
import com.mango.framework.BeanMappingService;
import com.mango.service.DailyRecordService;
import com.mango.service.StockReceiptService;
import com.mango.service.StockReceiptSkuService;
import com.mango.utils.ApplicationException;
import com.mango.utils.Assert;
import com.mango.utils.EntityUtils;
import lombok.extern.log4j.Log4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.List;

@Service
@Log4j
@Transactional(rollbackFor=Exception.class)
public class StockReceiptSkuServiceImpl implements StockReceiptSkuService {


    @Autowired
    StockReceiptSkuDao dao;
    @Autowired
    private BeanMappingService beanMappingService;
    @Autowired
    private DailyRecordService dailyRecordService;
    @Autowired
    private StockReceiptService stockReceiptService;

    @Override
    public int create(StockReceiptSkuDto dto) {
        try {
            StockReceiptSkuDto.checkAndTransfer(dto);
            stockReceiptService.checkPermission(dto.getReceiptId(), false);
            StockReceiptSku entity = beanMappingService.transform(dto, StockReceiptSku.class);
            EntityUtils.init(entity);
            dao.create(entity);

            dailyRecordService.create(DailyRecordTypeEnum.CREATE_RECEIPT_SKU_S.getCode(), DailyRecordTypeEnum.CREATE_RECEIPT_SKU_S.getDesc());
            return entity.getId();
        }catch (Exception e) {
            dailyRecordService.create(DailyRecordTypeEnum.CREATE_RECEIPT_SKU_E.getCode(), StringUtils.join(Lists.newArrayList(DailyRecordTypeEnum.CREATE_RECEIPT_SKU_E.getDesc(), e.getMessage()), ","));
            throw e;
        }

    }

    @Override
    public int update(StockReceiptSkuDto dto) {
        try {
            StockReceiptSkuDto.checkAndTransfer(dto);
            stockReceiptService.checkPermission(dto.getReceiptId(), false);
            StockReceiptSku entity = beanMappingService.transform(dto, StockReceiptSku.class);
            EntityUtils.update(entity);
            dao.update(entity);

            dailyRecordService.create(DailyRecordTypeEnum.UPDATE_RECEIPT_SKU_S.getCode(), DailyRecordTypeEnum.UPDATE_RECEIPT_SKU_S.getDesc());
            return entity.getId();
        }catch (Exception e) {
            dailyRecordService.create(DailyRecordTypeEnum.UPDATE_RECEIPT_SKU_E.getCode(), StringUtils.join(Lists.newArrayList(DailyRecordTypeEnum.UPDATE_RECEIPT_SKU_E.getDesc(), e.getMessage()), ","));
            throw e;
        }
    }

    @Override
    public boolean delete(List<Integer> ids) {
        try {
            Assert.assertFalse(CollectionUtils.isEmpty(ids),"请选择要删除的记录！");

            StockReceiptSku receiptSku = dao.findById(ids.get(0));
            stockReceiptService.checkPermission(receiptSku.getReceiptId(), false);
            dao.batchDeleteByIds(ids, new Date());

            dailyRecordService.create(DailyRecordTypeEnum.DELETE_RECEIPT_SKU_S.getCode(), DailyRecordTypeEnum.DELETE_RECEIPT_SKU_S.getDesc());
            return true;
        }catch (Exception e) {
            dailyRecordService.create(DailyRecordTypeEnum.DELETE_RECEIPT_SKU_E.getCode(), StringUtils.join(Lists.newArrayList(DailyRecordTypeEnum.DELETE_RECEIPT_SKU_E.getDesc(), e.getMessage()), ","));
            throw e;
        }
    }


    @Override
    public StockReceiptSkuDto findById(Integer id) {
        return beanMappingService.transform(dao.findById(id), StockReceiptSkuDto.class);
    }

    @Override
    public List<StockReceiptSkuDto> queryByCondition(QueryCondition queryCondition) {
        try {
            QueryCondition.checkAndTransfer(queryCondition);
            List<StockReceiptSku> entitys = dao.queryByCondition(queryCondition);
            List<StockReceiptSkuDto> dtos = Lists.newArrayList();
            if(!CollectionUtils.isEmpty(entitys)) {
                for(StockReceiptSku entity : entitys) {
                    StockReceiptSkuDto dto = beanMappingService.transform(entity, StockReceiptSkuDto.class);
                    StockReceiptSkuDto.convert(dto);
                    dtos.add(dto);
                }
            }

            return dtos;
        }catch (Exception e) {
            throw e;
        }
    }

    @Override
    public List<StockReceiptSkuDto> queryList(QueryCondition queryCondition) {
        try {
            QueryCondition.checkAndTransfer(queryCondition);
            List<StockReceiptSku> entitys = dao.queryList(queryCondition);
            List<StockReceiptSkuDto> dtos = Lists.newArrayList();
            if(!CollectionUtils.isEmpty(entitys)) {
                for(StockReceiptSku entity : entitys) {
                    StockReceiptSkuDto dto = beanMappingService.transform(entity, StockReceiptSkuDto.class);
                    StockReceiptSkuDto.convert(dto);
                    dtos.add(dto);
                }
            }

            return dtos;
        }catch (Exception e) {
            throw e;
        }
    }

    @Override
    public int queryCountByCondition(QueryCondition queryCondition) {
        try {
            QueryCondition.checkAndTransfer(queryCondition);
            int total = dao.queryCountByCondition(queryCondition);

            return total;
        }catch (Exception e) {
            throw e;
        }
    }

    @Override
    public StockReceiptSkuWebDto query(QueryCondition queryCondition) {
        try {
            StockReceiptSkuWebDto webDto = StockReceiptSkuWebDto.builder()
                    .total(queryCountByCondition(queryCondition))
                    .list(queryByCondition(queryCondition))
                    .build();
            dailyRecordService.create(DailyRecordTypeEnum.QUERY_RECEIPT_SKU_S.getCode(), DailyRecordTypeEnum.QUERY_RECEIPT_SKU_S.getDesc());
            return webDto;
        } catch (Exception e) {
            dailyRecordService.create(DailyRecordTypeEnum.QUERY_RECEIPT_SKU_E.getCode(), StringUtils.join(Lists.newArrayList(DailyRecordTypeEnum.QUERY_RECEIPT_SKU_E.getDesc(), e.getMessage()), ","));
            throw e;
        }
    }



}
