package com.mango.service.impl;


import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.mango.context.ContextContainer;
import com.mango.context.UserContext;
import com.mango.dao.UserDao;
import com.mango.dto.*;
import com.mango.entity.User;
import com.mango.enums.DailyRecordTypeEnum;
import com.mango.framework.BeanMappingService;
import com.mango.service.DailyRecordService;
import com.mango.service.ShopService;
import com.mango.service.UserService;
import com.mango.utils.*;
import lombok.extern.log4j.Log4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Log4j
public class UserServiceImpl implements UserService {


    @Autowired
    UserDao dao;
    @Autowired
    private BeanMappingService beanMappingService;
    @Autowired
    private DailyRecordService dailyRecordService;
    @Autowired
    private ShopService shopService;

    @Override
    public int create(UserDto dto) {
        //账户名唯一
        try {
            UserDto.checkAndTransfer(dto);
            Assert.assertFalse(com.mysql.jdbc.StringUtils.isNullOrEmpty(dto.getPassword()), "请填写密码！");
            Assert.assertFalse(dto.getPassword().trim().length() < 8, "密码最少 8 位！");
            User entity = beanMappingService.transform(dto, User.class);
            entity.setPassword(MD5Util.MD5(entity.getPassword()));
            EntityUtils.init(entity);
            dao.create(entity);

            DailyRecordDto dailyRecordDto = DailyRecordDto.builder().accountName(entity.getAccountName()).type(DailyRecordTypeEnum.REGISTER_S.getCode()).direction(DailyRecordTypeEnum.REGISTER_S.getDesc()).build();
            dailyRecordDto.setShopId(entity.getShopId());
            dailyRecordService.create(dailyRecordDto);
            return entity.getId();
        }catch (Exception e) {
            dailyRecordService.create(DailyRecordDto.builder()
                    .accountName(dto.getIp())
                    .type(DailyRecordTypeEnum.REGISTER_E.getCode())
                    .direction(StringUtils.join(Lists.newArrayList(DailyRecordTypeEnum.REGISTER_E.getDesc(), dto.getAccountName(), e.getMessage()), ",")).build());
            throw e;
        }

    }

    @Override
    public int update(UserDto dto) {
        //账户名唯一
        try {
            UserDto.checkAndTransfer(dto);
            List<User> sameNameUsers = dao.findByAccountName(dto.getAccountName());
            Assert.assertFalse(!CollectionUtils.isEmpty(sameNameUsers) && sameNameUsers.size() > 1 , "该帐号已存在！");
            List<Integer> ids = sameNameUsers.stream().map(c->c.getId()).collect(Collectors.toList());
            ids.remove(dto.getId());
            Assert.assertFalse(!CollectionUtils.isEmpty(ids), "该帐号已存在！");
            User entity = beanMappingService.transform(dto, User.class);
            EntityUtils.update(entity);
            dao.update(entity);

            return entity.getId();
        }catch (Exception e) {
            dailyRecordService.create(DailyRecordDto.builder()
                    .accountName(dto.getIp())
                    .type(DailyRecordTypeEnum.REGISTER_E.getCode())
                    .direction(StringUtils.join(Lists.newArrayList(DailyRecordTypeEnum.REGISTER_E.getDesc(), dto.getAccountName(), e.getMessage()), ",")).build());
            throw e;
        }

    }

    @Override
    public UserDto login(UserDto dto, HttpSession httpSession, UserContext userContext, HttpServletRequest httpServletRequest) {

        try {
            // TODO: 2017/7/21 如果存在多个用户了，这里要改
            List<User> entitys = dao.queryByConditionWithAnd(User.builder().accountName(dto.getAccountName()).password(MD5Util.MD5(dto.getPassword())).build());

            Assert.assertFalse(CollectionUtils.isEmpty(entitys) , "帐号或密码输入不正确！");
            Assert.assertFalse(entitys.size() > 1 , "获取帐号信息失败，请尽快联系管理员！");

            User entity = entitys.get(0);
            entity.setExtend2(DateUtils.formatSecond(new Date()));//最近登陆时间
            dao.updateLoginTime(entity);
            entity.setExtend3(shopService.findById(entity.getShopId()).getExtend3());
            enrichUserInfo(entity.getId(), httpSession, userContext, httpServletRequest);

            DailyRecordDto dailyRecordDto = DailyRecordDto.builder().accountName(entity.getAccountName()).type(DailyRecordTypeEnum.LOGIN_S.getCode()).direction(DailyRecordTypeEnum.LOGIN_S.getDesc()).build();
            dailyRecordDto.setShopId(entity.getShopId());
            dailyRecordService.create(dailyRecordDto);

            return beanMappingService.transform(entity, UserDto.class);
        } catch (Exception e) {
            dailyRecordService.create(DailyRecordDto.builder()
                    .accountName(dto.getIp())
                    .type(DailyRecordTypeEnum.LOGIN_E.getCode())
                    .direction(StringUtils.join(Lists.newArrayList(DailyRecordTypeEnum.LOGIN_E.getDesc(), dto.getAccountName(), e.getMessage()), ",")).build());
            throw e;
        }
    }

    @Override
    public boolean logout(HttpSession httpSession) {
        try {
            dailyRecordService.create(DailyRecordTypeEnum.LOGOUT_S.getCode(), DailyRecordTypeEnum.LOGOUT_S.getDesc());
            removeAttribute(httpSession);
            return Boolean.TRUE;
        } catch (Exception e) {
            dailyRecordService.create(DailyRecordTypeEnum.LOGOUT_E.getCode(), DailyRecordTypeEnum.LOGOUT_E.getDesc());
            throw e;
        }
    }

    @Override
    public boolean changePassword(UserDto userDto, HttpSession httpSession) {
        try {
            UserContext userContext = ContextContainer.getUserContext();

            Assert.assertFalse(Strings.isNullOrEmpty(userDto.getPassword()),"请输入原始密码！");
            Assert.assertFalse(Strings.isNullOrEmpty(userDto.getNewPassword()),"请输入新密码！");

            User user = dao.findById(userContext.getUserId());
            UserDto dto = beanMappingService.transform(user, UserDto.class);
            Assert.assertFalse(!user.getPassword().trim().equals(MD5Util.MD5(userDto.getPassword().trim())),"原始密码输入错误！");
            dto.setUpdateTime(new Date());
            dto.setPassword(MD5Util.MD5(userDto.getNewPassword().trim()));
            dao.changePassword(beanMappingService.transform(dto, User.class));
            dailyRecordService.create(DailyRecordTypeEnum.UPDATE_PASSWORD_S.getCode(), DailyRecordTypeEnum.UPDATE_PASSWORD_S.getDesc());
            removeAttribute(httpSession);
            return Boolean.TRUE;
        } catch (Exception e) {
            dailyRecordService.create(DailyRecordTypeEnum.UPDATE_PASSWORD_E.getCode(), DailyRecordTypeEnum.UPDATE_PASSWORD_E.getDesc());
            throw e;
        }
    }

    @Override
    public boolean resetPassword(UserDto userDto, HttpSession httpSession) {
        try {
            UserContext userContext = ContextContainer.getUserContext();
            // TODO: 2017/7/27 这里是传来的shopId，用户多了需要更改
            UserDto dto = beanMappingService.transform(dao.findByShopId(userDto.getId()).get(0), UserDto.class);
            dto.setPassword(MD5Util.MD5(userDto.getNewPassword().trim()));
            dto.setUpdateTime(new Date());
            dao.changePassword(beanMappingService.transform(dto, User.class));
            dailyRecordService.create(DailyRecordTypeEnum.RESET_PASSWORD_S.getCode(), DailyRecordTypeEnum.RESET_PASSWORD_S.getDesc() + "->" + dto.getAccountName());
            DailyRecordDto dailyRecordDto = DailyRecordDto.builder()
                    .accountName(dto.getAccountName())
                    .type(DailyRecordTypeEnum.RESET_PASSWORD_S.getCode())
                    .direction(userContext.getAccountName() +"->"+ DailyRecordTypeEnum.RESET_PASSWORD_S.getDesc()).build();
            dailyRecordDto.setShopId(dto.getShopId());
            dailyRecordService.create(dailyRecordDto);
            return Boolean.TRUE;
        } catch (Exception e) {
            dailyRecordService.create(DailyRecordTypeEnum.RESET_PASSWORD_E.getCode(), DailyRecordTypeEnum.RESET_PASSWORD_E.getDesc());
            throw e;
        }
    }

    @Override
    public UserDto userInfo(HttpSession httpSession) {
        try {
            UserContext userContext = ContextContainer.getUserContext();
            UserDto dto = beanMappingService.transform(dao.findById(userContext.getUserId()), UserDto.class);
            dailyRecordService.create(DailyRecordTypeEnum.USERINFO_S.getCode(), DailyRecordTypeEnum.USERINFO_S.getDesc());
            return dto;
        } catch (Exception e) {
            dailyRecordService.create(DailyRecordTypeEnum.USERINFO_E.getCode(), DailyRecordTypeEnum.USERINFO_E.getDesc());
            throw e;
        }
    }

    @Override
    public UserDto findById(Integer id) {
        return beanMappingService.transform(dao.findById(id), UserDto.class);
    }

    @Override
    public List<UserDto> queryByConditionWithAnd(UserDto dto) {
        List<User> entitys = dao.queryByConditionWithAnd(User.builder().accountName(dto.getAccountName()).ip(dto.getIp()).build());
        if(CollectionUtils.isEmpty(entitys)) return Lists.newArrayList();
        return Lists.transform(entitys, entity -> beanMappingService.transform(entity, UserDto.class));
    }

    @Override
    public void enrichUserInfo(Integer userId, HttpSession httpSession, UserContext userContext, HttpServletRequest request) {
        httpSession.setAttribute("userInfo", StringUtils.join(Lists.newArrayList(userId, request.getRemoteAddr(),request.getHeader("User-Agent")), ","));
        userContext.setUserId(userId);
    }


    private boolean removeAttribute(HttpSession httpSession) {
        Enumeration<String> attributeNames = httpSession.getAttributeNames();
        List<String> names = Collections.list(attributeNames);
        names.forEach(httpSession::removeAttribute);
        return true;
    }


    @Override
    public List<UserDto> queryByCondition(QueryCondition queryCondition) {
        try {
            QueryCondition.checkAndTransfer(queryCondition);
            List<User> entitys = dao.queryByCondition(queryCondition);
            List<UserDto> dtos = Lists.newArrayList();
            if(!CollectionUtils.isEmpty(entitys)) {
                for(User entity : entitys) {
                    UserDto dto = beanMappingService.transform(entity, UserDto.class);
                    UserDto.convert(dto);
                    dtos.add(dto);
                }
            }

            return dtos;
        }catch (Exception e) {
            throw e;
        }
    }

    @Override
    public int queryCountByCondition(QueryCondition queryCondition) {
        try {
            QueryCondition.checkAndTransfer(queryCondition);
            int total = dao.queryCountByCondition(queryCondition);

            return total;
        }catch (Exception e) {
            throw e;
        }
    }

    @Override
    public UserWebDto query(QueryCondition queryCondition) {
        try {
            UserWebDto webDto = UserWebDto.builder()
                    .total(queryCountByCondition(queryCondition))
                    .list(queryByCondition(queryCondition))
                    .build();
            dailyRecordService.create(DailyRecordTypeEnum.QUERY_USER_S.getCode(), DailyRecordTypeEnum.QUERY_USER_S.getDesc());
            return webDto;
        } catch (Exception e) {
            dailyRecordService.create(DailyRecordTypeEnum.QUERY_USER_E.getCode(), StringUtils.join(Lists.newArrayList(DailyRecordTypeEnum.QUERY_USER_E.getDesc(), e.getMessage()), ","));
            throw e;
        }
    }
}
