package com.mango.service.impl;


import com.google.common.collect.Lists;
import com.mango.context.ContextContainer;
import com.mango.context.UserContext;
import com.mango.dao.InventoryChangeFlowDao;
import com.mango.dto.*;
import com.mango.entity.InventoryChangeFlow;
import com.mango.entity.StockReceipt;
import com.mango.enums.DailyRecordTypeEnum;
import com.mango.framework.BeanMappingService;
import com.mango.service.DailyRecordService;
import com.mango.service.InventoryChangeFlowService;
import com.mango.service.StockReceiptService;
import com.mango.utils.ApplicationException;
import com.mango.utils.DateUtils;
import com.mango.utils.EntityUtils;
import lombok.extern.log4j.Log4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Log4j
@Transactional(rollbackFor=Exception.class)
public class InventoryChangeFlowServiceImpl implements InventoryChangeFlowService {


    @Autowired
    InventoryChangeFlowDao dao;
    @Autowired
    private BeanMappingService beanMappingService;
    @Autowired
    private DailyRecordService dailyRecordService;
    @Autowired
    private StockReceiptService stockReceiptService;


    @Override
    public int create(InventoryChangeFlowDto dto) {
        try {
            InventoryChangeFlowDto.checkAndTransfer(dto);

            InventoryChangeFlow entity = beanMappingService.transform(dto, InventoryChangeFlow.class);
            EntityUtils.init(entity);
            dao.create(entity);
            dailyRecordService.create(DailyRecordTypeEnum.CREATE_INVENTORY_FLOW_S.getCode(), DailyRecordTypeEnum.CREATE_INVENTORY_FLOW_S.getDesc());
            return entity.getId();
        }catch (Exception e) {
            dailyRecordService.create(DailyRecordTypeEnum.CREATE_INVENTORY_FLOW_E.getCode(), StringUtils.join(Lists.newArrayList(DailyRecordTypeEnum.CREATE_INVENTORY_FLOW_E.getDesc(), e.getMessage()), ","));
            throw e;
        }

    }

    @Override
    public int update(InventoryChangeFlowDto dto) {
        try {
            InventoryChangeFlowDto.checkAndTransfer(dto);
            InventoryChangeFlow entity = beanMappingService.transform(dto, InventoryChangeFlow.class);
            EntityUtils.update(entity);
            dao.update(entity);

            dailyRecordService.create(DailyRecordTypeEnum.UPDATE_INVENTORY_FLOW_S.getCode(), DailyRecordTypeEnum.UPDATE_INVENTORY_FLOW_S.getDesc());
            return entity.getId();
        }catch (Exception e) {
            dailyRecordService.create(DailyRecordTypeEnum.UPDATE_INVENTORY_FLOW_E.getCode(), StringUtils.join(Lists.newArrayList(DailyRecordTypeEnum.UPDATE_INVENTORY_FLOW_E.getDesc(), e.getMessage()), ","));
            throw e;
        }
    }


    @Override
    public InventoryChangeFlowDto findById(Integer id) {
        InventoryChangeFlowDto dto = beanMappingService.transform(dao.findById(id), InventoryChangeFlowDto.class);
        return dto;
    }

    @Override
    public List<InventoryChangeFlowDto> queryByCondition(QueryCondition queryCondition) {
        try {
            QueryCondition.checkAndTransfer(queryCondition);
            List<InventoryChangeFlow> entitys = dao.queryByCondition(queryCondition);
            List<InventoryChangeFlowDto> dtos = Lists.newArrayList();
            if(!CollectionUtils.isEmpty(entitys)) {
                for(InventoryChangeFlow entity : entitys) {
                    InventoryChangeFlowDto dto = beanMappingService.transform(entity, InventoryChangeFlowDto.class);
                    InventoryChangeFlowDto.convert(dto);
                    dtos.add(dto);
                }
            }

            return dtos;
        }catch (Exception e) {
            throw e;
        }
    }

    @Override
    public int queryCountByCondition(QueryCondition queryCondition) {
        try {
            QueryCondition.checkAndTransfer(queryCondition);
            int total = dao.queryCountByCondition(queryCondition);

            return total;
        }catch (Exception e) {
            throw e;
        }
    }

    @Override
    public InventoryChangeFlowWebDto query(QueryCondition queryCondition) {
        try {
            InventoryChangeFlowWebDto webDto = InventoryChangeFlowWebDto.builder()
                    .total(queryCountByCondition(queryCondition))
                    .list(queryByCondition(queryCondition))
                    .build();
            dailyRecordService.create(DailyRecordTypeEnum.QUERY_INVENTORY_FLOW_S.getCode(), DailyRecordTypeEnum.QUERY_INVENTORY_FLOW_S.getDesc());
            return webDto;
        } catch (Exception e) {
            dailyRecordService.create(DailyRecordTypeEnum.QUERY_INVENTORY_FLOW_E.getCode(), StringUtils.join(Lists.newArrayList(DailyRecordTypeEnum.QUERY_INVENTORY_FLOW_E.getDesc(), e.getMessage()), ","));
            throw e;
        }
    }

    @Override
    public List<InventoryChangeFlowCollectDto> queryCollect(QueryCondition queryCondition) {
        try {

            List<Date> list = Lists.newArrayList();
            Date endDay = DateUtils.formatNextDayStart(new Date());
            Date startDay = DateUtils.getFrontDate(endDay, 1);
            while (startDay.compareTo(endDay) <= 0) {
                list.add(startDay);
                startDay = DateUtils.formatNextDayStart(startDay);
            }

            //找出start-》end内的所有单据
            UserContext userContext = ContextContainer.getUserContext();
            List<StockReceiptDto> stockReceipts = stockReceiptService.queryByCondition(QueryCondition.builder()
                    .receiptType(queryCondition.getReceiptType())
                    .shopId(userContext.getShopId())
                    .build());

            stockReceipts = stockReceipts.stream().filter(c->c.getTotalCount() != null && c.getTotalCount() > 0 && !c.getStatus().equals(1)).collect(Collectors.toList());

            List<InventoryChangeFlowCollectDto> dtos = Lists.newArrayList();
            if(CollectionUtils.isEmpty(stockReceipts)) return dtos;
            for(int i = 0; i < list.size(); i++) {

                Date start = list.get(i);
                Date end = i < list.size() - 1 ? list.get(i+1) : new Date();

                List<StockReceiptDto> receiptDtos = stockReceipts.stream().filter(c->(c.getStockChangeTime()).compareTo(start) >= 0 && (c.getStockChangeTime()).compareTo(end) <= 0).collect(Collectors.toList());
                List<InventoryChangeFlowDataDto> data = Lists.newArrayList();
                if(!CollectionUtils.isEmpty(receiptDtos)) {
                    receiptDtos.forEach(c->{
                        data.add(InventoryChangeFlowDataDto.builder().time(c.getStockChangeTime()).count(c.getTotalCount()).price(c.getTotalMoney()).build());
                    });
                }
                InventoryChangeFlowCollectDto dto = InventoryChangeFlowCollectDto.builder()
                        .date(start)
                        .data(data)
                        .build();
                dtos.add(dto);
            }


            dailyRecordService.create(DailyRecordTypeEnum.QUERYCOLLECT_INVENTORY_FLOW_S.getCode(), DailyRecordTypeEnum.QUERYCOLLECT_INVENTORY_FLOW_S.getDesc());
            return dtos;
        } catch (Exception e) {
            dailyRecordService.create(DailyRecordTypeEnum.QUERYCOLLECT_INVENTORY_FLOW_E.getCode(), StringUtils.join(Lists.newArrayList(DailyRecordTypeEnum.QUERYCOLLECT_INVENTORY_FLOW_E.getDesc(), e.getMessage()), ","));
            throw e;
        }
    }


}
