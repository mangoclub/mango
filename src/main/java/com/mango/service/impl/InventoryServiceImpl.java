package com.mango.service.impl;


import com.google.common.collect.Lists;
import com.mango.dao.InventoryDao;
import com.mango.dto.QueryCondition;
import com.mango.dto.InventoryDto;
import com.mango.dto.InventoryWebDto;
import com.mango.entity.Inventory;
import com.mango.enums.DailyRecordTypeEnum;
import com.mango.framework.BeanMappingService;
import com.mango.service.DailyRecordService;
import com.mango.service.InventoryService;
import com.mango.utils.ApplicationException;
import com.mango.utils.EntityUtils;
import lombok.extern.log4j.Log4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Service
@Log4j
@Transactional(rollbackFor=Exception.class)
public class InventoryServiceImpl implements InventoryService {


    @Autowired
    InventoryDao dao;
    @Autowired
    private BeanMappingService beanMappingService;
    @Autowired
    private DailyRecordService dailyRecordService;


    @Override
    public int create(InventoryDto dto) {
        try {
            InventoryDto.checkAndTransfer(dto);

            Inventory entity = beanMappingService.transform(dto, Inventory.class);
            EntityUtils.init(entity);
            dao.create(entity);
            dailyRecordService.create(DailyRecordTypeEnum.CREATE_INVENTORY_S.getCode(), DailyRecordTypeEnum.CREATE_INVENTORY_S.getDesc());
            return entity.getId();
        }catch (Exception e) {
            dailyRecordService.create(DailyRecordTypeEnum.CREATE_INVENTORY_E.getCode(), StringUtils.join(Lists.newArrayList(DailyRecordTypeEnum.CREATE_INVENTORY_E.getDesc(), e.getMessage()), ","));
            throw e;
        }

    }

    @Override
    public int update(InventoryDto dto) {
        try {
            InventoryDto.checkAndTransfer(dto);
            Inventory entity = beanMappingService.transform(dto, Inventory.class);
            EntityUtils.update(entity);
            dao.update(entity);

            dailyRecordService.create(DailyRecordTypeEnum.UPDATE_INVENTORY_S.getCode(), DailyRecordTypeEnum.UPDATE_INVENTORY_S.getDesc());
            return entity.getId();
        }catch (Exception e) {
            dailyRecordService.create(DailyRecordTypeEnum.UPDATE_INVENTORY_E.getCode(), StringUtils.join(Lists.newArrayList(DailyRecordTypeEnum.UPDATE_INVENTORY_E.getDesc(), e.getMessage()), ","));
            throw e;
        }
    }


    @Override
    public InventoryDto findById(Integer id) {
        InventoryDto dto = beanMappingService.transform(dao.findById(id), InventoryDto.class);
        return dto;
    }

    @Override
    public List<InventoryDto> queryByCondition(QueryCondition queryCondition) {
        try {
            QueryCondition.checkAndTransfer(queryCondition);
            List<Inventory> entitys = dao.queryByCondition(queryCondition);
            List<InventoryDto> dtos = Lists.newArrayList();
            if(!CollectionUtils.isEmpty(entitys)) {
                for(Inventory entity : entitys) {
                    InventoryDto dto = beanMappingService.transform(entity, InventoryDto.class);
                    InventoryDto.convert(dto);
                    dtos.add(dto);
                }
            }

            return dtos;
        }catch (Exception e) {
            throw e;
        }
    }

    @Override
    public int queryCountByCondition(QueryCondition queryCondition) {
        try {
            QueryCondition.checkAndTransfer(queryCondition);
            int total = dao.queryCountByCondition(queryCondition);

            return total;
        }catch (Exception e) {
            throw e;
        }
    }

    @Override
    public InventoryWebDto query(QueryCondition queryCondition) {
        try {
            InventoryWebDto webDto = InventoryWebDto.builder()
                    .total(queryCountByCondition(queryCondition))
                    .list(queryByCondition(queryCondition))
                    .build();
            dailyRecordService.create(DailyRecordTypeEnum.QUERY_INVENTORY_S.getCode(), DailyRecordTypeEnum.QUERY_INVENTORY_S.getDesc());
            return webDto;
        } catch (Exception e) {
            dailyRecordService.create(DailyRecordTypeEnum.QUERY_INVENTORY_E.getCode(), StringUtils.join(Lists.newArrayList(DailyRecordTypeEnum.QUERY_INVENTORY_E.getDesc(), e.getMessage()), ","));
            throw e;
        }
    }

    @Override
    public List<InventoryDto> queryInventory(Integer shopId, List<Integer> productIds) {
        List<Inventory> inventories = dao.queryInventory(shopId, productIds);
        if(CollectionUtils.isEmpty(inventories)) return Lists.newArrayList();
        return Lists.transform(inventories, inventory->beanMappingService.transform(inventory, InventoryDto.class));
    }

    @Override
    public InventoryDto queryInventoryByProductId(Integer shopId, Integer productId) {
        InventoryDto dto = InventoryDto.builder().build();
        Inventory entity = dao.queryInventoryByProductId(shopId, productId);
        if(entity != null) dto = beanMappingService.transform(entity, InventoryDto.class);
        return dto;
    }


}
