package com.mango.service.impl.distributedLock;

import com.mango.constant.Constants;
import com.mango.dao.DistributedLockDao;
import com.mango.entity.DistributedLock;
import com.mango.exception.LockFailException;
import com.mango.utils.Assert;
import com.mango.utils.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component("mySqlDistributedLockHandler")
public class MySqlDistributedLockHandler implements DistributedLockHandler {

    @Autowired
    DistributedLockDao distributedLockDao;

    @Override
    public void lock(String key, Integer type, Date expiredTime) {

        try {
            DistributedLock distributedLock = DistributedLock.builder()
                    .expiredTime(expiredTime)
                    .key(key)
                    .type(type)
                    .build();
            EntityUtils.init(distributedLock);
            distributedLockDao.create(distributedLock);
        } catch (DuplicateKeyException e) {
            throw new LockFailException(Constants.FAIL_GET_LOCK,e);
        }
    }

    @Override
    public void unlock(String key, Integer type) {
        DistributedLock distributedLock = distributedLockDao.findByKeyAndType(key,type);
        Assert.assertFalse(distributedLock==null,String.format("type:[%s],key:[%s]不存在",type,key));
        distributedLockDao.remove(distributedLock);
    }


}
