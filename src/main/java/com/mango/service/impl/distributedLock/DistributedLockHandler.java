package com.mango.service.impl.distributedLock;

import java.util.Date;


public interface DistributedLockHandler {

    void lock(String key, Integer type, Date expiredTime);

    void unlock(String key, Integer type);


}
