package com.mango.service.impl;


import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.mango.context.ContextContainer;
import com.mango.context.UserContext;
import com.mango.dao.ShopDao;
import com.mango.dao.UserDao;
import com.mango.dto.DailyRecordDto;
import com.mango.dto.QueryCondition;
import com.mango.dto.ShopDto;
import com.mango.dto.ShopWebDto;
import com.mango.entity.Shop;
import com.mango.entity.User;
import com.mango.enums.DailyRecordTypeEnum;
import com.mango.framework.BeanMappingService;
import com.mango.service.DailyRecordService;
import com.mango.service.ShopService;
import com.mango.utils.*;
import lombok.extern.log4j.Log4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.servlet.http.HttpSession;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Log4j
public class ShopServiceImpl implements ShopService {


    @Autowired
    ShopDao dao;
    @Autowired
    UserDao userDao;
    @Autowired
    private BeanMappingService beanMappingService;
    @Autowired
    private DailyRecordService dailyRecordService;

    @Override
    @Transactional(rollbackFor=Exception.class)
    public int create(ShopDto shopDto) {
        //账户名唯一
        try {
            ShopDto.checkAndTransfer(shopDto);
            Assert.assertFalse(com.mysql.jdbc.StringUtils.isNullOrEmpty(shopDto.getPassword()), "请填写密码！");
            Assert.assertFalse(shopDto.getPassword().trim().length() < 8, "密码最少 8 位！");
            List<Shop> sameNameShops = dao.findByAccountName(shopDto.getAccountName());
            Assert.assertFalse(!CollectionUtils.isEmpty(sameNameShops) , "该帐号已被注册！");
//            List<Shop> samenIpShops = dao.queryByConditionWithAnd(Shop.builder().ip(shopDto.getIp()).build());
//            Assert.assertFalse(!CollectionUtils.isEmpty(samenIpShops) , "不可重复注册，请使用已注册帐号名！");
            Shop shop = beanMappingService.transform(shopDto, Shop.class);
            shop.setPassword(MD5Util.MD5(shop.getPassword()));
            shop.setExtend3(DateUtils.formatSecond(DateUtils.getBackDate(DateUtils.formatNextDayStart(new Date()), 120)));
            EntityUtils.init(shop);
            dao.create(shop);
            User user = User.builder()
                    .accountName(shopDto.getAccountName())
                    .password(shop.getPassword())
                    .phone(shopDto.getPhone())
                    .ip(shopDto.getIp())
                    .userAgent(shopDto.getUserAgent())
                    .build();
            EntityUtils.init(user);
            user.setShopId(shop.getId());
            userDao.create(user);
            DailyRecordDto dailyRecordDto = DailyRecordDto.builder().accountName(shop.getAccountName()).type(DailyRecordTypeEnum.REGISTER_S.getCode()).direction(DailyRecordTypeEnum.REGISTER_S.getDesc()).build();
            dailyRecordDto.setShopId(shop.getId());
            dailyRecordService.create(dailyRecordDto);


            return shop.getId();
        }catch (Exception e) {
            dailyRecordService.create(DailyRecordDto.builder()
                    .accountName(shopDto.getIp())
                    .type(DailyRecordTypeEnum.REGISTER_E.getCode())
                    .direction(StringUtils.join(Lists.newArrayList(DailyRecordTypeEnum.REGISTER_E.getDesc(), shopDto.getAccountName(), e.getMessage()), ",")).build());
            throw e;
        }

    }

    @Override
    @Transactional(rollbackFor=Exception.class)
    public int update(ShopDto shopDto) {
        //账户名唯一
        try {
            ShopDto.checkAndTransfer(shopDto);
            List<Shop> sameNameShops = dao.findByAccountName(shopDto.getAccountName());
            Assert.assertFalse(!CollectionUtils.isEmpty(sameNameShops) && sameNameShops.size() > 1 , "该帐号已存在！");
            List<Integer> ids = sameNameShops.stream().map(c->c.getId()).collect(Collectors.toList());
            ids.remove(shopDto.getId());
            Assert.assertFalse(!CollectionUtils.isEmpty(ids), "该帐号已存在！");
            Shop shop = beanMappingService.transform(shopDto, Shop.class);
            EntityUtils.update(shop);
            dao.update(shop);

            dailyRecordService.create(DailyRecordTypeEnum.UPDATE_SHOP_S.getCode(), DailyRecordTypeEnum.UPDATE_SHOP_S.getDesc());
            return shop.getId();
        }catch (Exception e) {
            dailyRecordService.create(DailyRecordTypeEnum.UPDATE_SHOP_E.getCode(), StringUtils.join(Lists.newArrayList(DailyRecordTypeEnum.UPDATE_SHOP_E.getDesc(), e.getMessage()), ","));
            throw e;
        }

    }

    @Override
    @Transactional(rollbackFor=Exception.class)
    public int extend(ShopDto shopDto) {
        try {
            Shop shop = beanMappingService.transform(shopDto, Shop.class);
            EntityUtils.update(shop);
            dao.extend(shop);

            dailyRecordService.create(DailyRecordTypeEnum.UPDATE_SHOP_S.getCode(), DailyRecordTypeEnum.UPDATE_SHOP_S.getDesc());
            return shop.getId();
        }catch (Exception e) {
            dailyRecordService.create(DailyRecordTypeEnum.UPDATE_SHOP_E.getCode(), StringUtils.join(Lists.newArrayList(DailyRecordTypeEnum.UPDATE_SHOP_E.getDesc(), e.getMessage()), ","));
            throw e;
        }

    }

    @Override
    public ShopDto login(ShopDto shopDto, HttpSession httpSession, UserContext userContext) {

        try {
            List<Shop> shops = dao.queryByConditionWithAnd(Shop.builder().accountName(shopDto.getAccountName()).password(MD5Util.MD5(shopDto.getPassword())).build());

            Assert.assertFalse(CollectionUtils.isEmpty(shops) , "帐号或密码输入不正确！");
            Assert.assertFalse(shops.size() > 1 , "获取帐号信息失败，请尽快联系管理员！");

            Shop shop = shops.get(0);
            enrichUserInfo(shop.getId(), httpSession, userContext);

            DailyRecordDto dailyRecordDto = DailyRecordDto.builder().accountName(shop.getAccountName()).type(DailyRecordTypeEnum.LOGIN_S.getCode()).direction(DailyRecordTypeEnum.LOGIN_S.getDesc()).build();
            dailyRecordDto.setShopId(shop.getId());
            dailyRecordService.create(dailyRecordDto);

            return beanMappingService.transform(shop, ShopDto.class);
        } catch (Exception e) {
            dailyRecordService.create(DailyRecordDto.builder()
                    .accountName(shopDto.getIp())
                    .type(DailyRecordTypeEnum.LOGIN_E.getCode())
                    .direction(StringUtils.join(Lists.newArrayList(DailyRecordTypeEnum.LOGIN_E.getDesc(), shopDto.getAccountName(), e.getMessage()), ",")).build());
            throw e;
        }
    }

    @Override
    public boolean logout(HttpSession httpSession) {
        try {
            dailyRecordService.create(DailyRecordTypeEnum.LOGOUT_S.getCode(), DailyRecordTypeEnum.LOGOUT_S.getDesc());
            removeAttribute(httpSession);
            return Boolean.TRUE;
        } catch (Exception e) {
            dailyRecordService.create(DailyRecordTypeEnum.LOGOUT_E.getCode(), DailyRecordTypeEnum.LOGOUT_E.getDesc());
            throw e;
        }
    }

    @Override
    public boolean changePassword(ShopDto dto, HttpSession httpSession) {
        try {
            UserContext userContext = ContextContainer.getUserContext();

            Assert.assertFalse(Strings.isNullOrEmpty(dto.getPassword()),"请输入原始密码！");
            Assert.assertFalse(Strings.isNullOrEmpty(dto.getNewPassword()),"请输入新密码！");

            ShopDto shopDto = beanMappingService.transform(dao.findById(userContext.getShopId()), ShopDto.class);
            Assert.assertFalse(!shopDto.getPassword().trim().equals(MD5Util.MD5(dto.getPassword()).trim()),"原始密码输入错误！");
            shopDto.setUpdateTime(new Date());
            shopDto.setPassword(MD5Util.MD5(dto.getNewPassword()).trim());
            dao.changePassword(beanMappingService.transform(shopDto, Shop.class));
            dailyRecordService.create(DailyRecordTypeEnum.UPDATE_PASSWORD_S.getCode(), DailyRecordTypeEnum.UPDATE_PASSWORD_S.getDesc());
            removeAttribute(httpSession);
            return Boolean.TRUE;
        } catch (Exception e) {
            dailyRecordService.create(DailyRecordTypeEnum.UPDATE_PASSWORD_E.getCode(), DailyRecordTypeEnum.UPDATE_PASSWORD_E.getDesc());
            throw e;
        }
    }

    @Override
    public boolean resetPassword(ShopDto dto, HttpSession httpSession) {
        try {
            UserContext userContext = ContextContainer.getUserContext();
            ShopDto shopDto = beanMappingService.transform(dao.findById(dto.getId()), ShopDto.class);
            shopDto.setPassword(MD5Util.MD5(dto.getNewPassword()).trim());
            shopDto.setUpdateTime(new Date());
            dao.changePassword(beanMappingService.transform(shopDto, Shop.class));
            dailyRecordService.create(DailyRecordTypeEnum.RESET_PASSWORD_S.getCode(), DailyRecordTypeEnum.RESET_PASSWORD_S.getDesc() + "->" + shopDto.getAccountName());
            DailyRecordDto dailyRecordDto = DailyRecordDto.builder()
                    .accountName(shopDto.getAccountName())
                    .type(DailyRecordTypeEnum.RESET_PASSWORD_S.getCode())
                    .direction(userContext.getAccountName() +"->"+ DailyRecordTypeEnum.RESET_PASSWORD_S.getDesc()).build();
            dailyRecordDto.setShopId(shopDto.getId());
            dailyRecordService.create(dailyRecordDto);
            return Boolean.TRUE;
        } catch (Exception e) {
            dailyRecordService.create(DailyRecordTypeEnum.RESET_PASSWORD_E.getCode(), DailyRecordTypeEnum.RESET_PASSWORD_E.getDesc());
            throw e;
        }
    }

    @Override
    public ShopDto userInfo(HttpSession httpSession) {
        try {
            UserContext userContext = ContextContainer.getUserContext();
            ShopDto shopDto = beanMappingService.transform(dao.findById(userContext.getShopId()), ShopDto.class);
            dailyRecordService.create(DailyRecordTypeEnum.USERINFO_S.getCode(), DailyRecordTypeEnum.USERINFO_S.getDesc());
            return shopDto;
        } catch (Exception e) {
            dailyRecordService.create(DailyRecordTypeEnum.USERINFO_E.getCode(), DailyRecordTypeEnum.USERINFO_E.getDesc());
            throw e;
        }
    }

    @Override
    public ShopDto findById(Integer id) {
        return beanMappingService.transform(dao.findById(id), ShopDto.class);
    }

    @Override
    public List<ShopDto> queryByConditionWithAnd(ShopDto shopDto) {
        List<Shop> shops = dao.queryByConditionWithAnd(Shop.builder().accountName(shopDto.getAccountName()).ip(shopDto.getIp()).build());
        if(CollectionUtils.isEmpty(shops)) return Lists.newArrayList();
        return Lists.transform(shops, shop -> beanMappingService.transform(shop, ShopDto.class));
    }

    @Override
    public void enrichUserInfo(Integer shopId, HttpSession httpSession, UserContext userContext) {
        httpSession.setAttribute("userInfo", StringUtils.join(Lists.newArrayList(shopId), ","));
        userContext.setShopId(shopId);
    }


    private boolean removeAttribute(HttpSession httpSession) {
        Enumeration<String> attributeNames = httpSession.getAttributeNames();
        List<String> names = Collections.list(attributeNames);
        names.forEach(httpSession::removeAttribute);
        return true;
    }


    @Override
    public List<ShopDto> queryByCondition(QueryCondition queryCondition) {
        try {
            QueryCondition.checkAndTransfer(queryCondition);
            List<Shop> entitys = dao.queryByCondition(queryCondition);
            List<ShopDto> dtos = Lists.newArrayList();
            if(!CollectionUtils.isEmpty(entitys)) {
                for(Shop entity : entitys) {
                    ShopDto dto = beanMappingService.transform(entity, ShopDto.class);
                    ShopDto.convert(dto);
                    dtos.add(dto);
                }
            }

            return dtos;
        }catch (Exception e) {
            throw e;
        }
    }

    @Override
    public int queryCountByCondition(QueryCondition queryCondition) {
        try {
            QueryCondition.checkAndTransfer(queryCondition);
            int total = dao.queryCountByCondition(queryCondition);

            return total;
        }catch (Exception e) {
            throw e;
        }
    }

    @Override
    public ShopWebDto query(QueryCondition queryCondition) {
        try {
            ShopWebDto webDto = ShopWebDto.builder()
                    .total(queryCountByCondition(queryCondition))
                    .list(queryByCondition(queryCondition))
                    .build();
            dailyRecordService.create(DailyRecordTypeEnum.QUERY_USER_S.getCode(), DailyRecordTypeEnum.QUERY_USER_S.getDesc());
            return webDto;
        } catch (Exception e) {
            dailyRecordService.create(DailyRecordTypeEnum.QUERY_USER_E.getCode(), StringUtils.join(Lists.newArrayList(DailyRecordTypeEnum.QUERY_USER_E.getDesc(), e.getMessage()), ","));
            throw e;
        }
    }
}
