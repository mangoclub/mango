package com.mango.service.impl;


import com.google.common.collect.Lists;
import com.mango.context.ContextContainer;
import com.mango.context.UserContext;
import com.mango.dao.ProductDao;
import com.mango.dto.ProductDto;
import com.mango.dto.ProductWebDto;
import com.mango.dto.QueryCondition;
import com.mango.entity.Product;
import com.mango.enums.DailyRecordTypeEnum;
import com.mango.framework.BeanMappingService;
import com.mango.service.AbstractBasicService;
import com.mango.service.DailyRecordService;
import com.mango.service.InventoryService;
import com.mango.service.ProductService;
import com.mango.utils.ApplicationException;
import com.mango.utils.Assert;
import com.mango.utils.EntityUtils;
import lombok.extern.log4j.Log4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Log4j
public class ProductServiceImpl extends AbstractBasicService<ProductDto> implements ProductService {


    @Autowired
    ProductDao dao;
    @Autowired
    private BeanMappingService beanMappingService;
    @Autowired
    private DailyRecordService dailyRecordService;
    @Autowired
    private InventoryService inventoryService;

    @Override
    public int create(ProductDto dto) {
        try {
            ProductDto.checkAndTransfer(dto);
            checkUnique(dto);
            Product entity = beanMappingService.transform(dto, Product.class);
            EntityUtils.init(entity);
            dao.create(entity);

            dailyRecordService.create(DailyRecordTypeEnum.CREATE_PRODUCT_S.getCode(), DailyRecordTypeEnum.CREATE_PRODUCT_S.getDesc());
            return entity.getId();
        }catch (Exception e) {
            dailyRecordService.create(DailyRecordTypeEnum.CREATE_PRODUCT_E.getCode(), StringUtils.join(Lists.newArrayList(DailyRecordTypeEnum.CREATE_PRODUCT_E.getDesc(), e.getMessage()), ","));
            throw e;
        }

    }

    @Override
    public int update(ProductDto dto) {
        try {
            ProductDto.checkAndTransfer(dto);
            checkUpdateUnique(dto);
            Product entity = beanMappingService.transform(dto, Product.class);
            EntityUtils.update(entity);
            dao.update(entity);

            dailyRecordService.create(DailyRecordTypeEnum.UPDATE_PRODUCT_S.getCode(), DailyRecordTypeEnum.UPDATE_PRODUCT_S.getDesc());
            return entity.getId();
        }catch (Exception e) {
            dailyRecordService.create(DailyRecordTypeEnum.UPDATE_PRODUCT_E.getCode(), StringUtils.join(Lists.newArrayList(DailyRecordTypeEnum.UPDATE_PRODUCT_E.getDesc(), e.getMessage()), ","));
            throw e;
        }
    }


    @Override
    public ProductDto findById(Integer id) {
        ProductDto dto = beanMappingService.transform(dao.findById(id), ProductDto.class);
        dto.setStockInventory(inventoryService.queryInventoryByProductId(dto.getShopId(), dto.getId()).getStockInventory());
        return dto;
    }

    @Override
    public List<ProductDto> queryByCondition(QueryCondition queryCondition) {
        try {
            QueryCondition.checkAndTransfer(queryCondition);
            List<Product> entitys = dao.queryByCondition(queryCondition);
            List<ProductDto> dtos = Lists.newArrayList();
            if(!CollectionUtils.isEmpty(entitys)) {
                for(Product entity : entitys) {
                    ProductDto dto = beanMappingService.transform(entity, ProductDto.class);
                    ProductDto.convert(dto);
                    dtos.add(dto);
                }
            }

            return dtos;
        }catch (Exception e) {
            throw e;
        }
    }

    @Override
    public int queryCountByCondition(QueryCondition queryCondition) {
        try {
            QueryCondition.checkAndTransfer(queryCondition);
            int total = dao.queryCountByCondition(queryCondition);

            return total;
        }catch (Exception e) {
            throw e;
        }
    }

    @Override
    public ProductWebDto query(QueryCondition queryCondition) {
        try {
            ProductWebDto webDto = ProductWebDto.builder()
                    .total(queryCountByCondition(queryCondition))
                    .list(queryByCondition(queryCondition))
                    .build();
            dailyRecordService.create(DailyRecordTypeEnum.QUERY_PRODUCT_S.getCode(), DailyRecordTypeEnum.QUERY_PRODUCT_S.getDesc());
            return webDto;
        } catch (Exception e) {
            dailyRecordService.create(DailyRecordTypeEnum.QUERY_PRODUCT_E.getCode(), StringUtils.join(Lists.newArrayList(DailyRecordTypeEnum.QUERY_PRODUCT_E.getDesc(), e.getMessage()), ","));
            throw e;
        }
    }

    private void checkUnique(ProductDto dto) {
        UserContext userContext = ContextContainer.getUserContext();
        List<Product> entitys = dao.queryByConditionWithAnd(Product.builder().shopId(userContext.getShopId()).code(dto.getCode()).build());
        Assert.assertFalse(!CollectionUtils.isEmpty(entitys) , "您的产品库中已存在相同编号的产品");
    }

    private void checkUpdateUnique(ProductDto dto) {
        UserContext userContext = ContextContainer.getUserContext();
        List<Product> entitys = dao.queryByConditionWithAnd(Product.builder().shopId(userContext.getShopId()).code(dto.getCode()).build());
        Assert.assertFalse(!CollectionUtils.isEmpty(entitys) && entitys.size() > 1, "您的产品库中已存在相同编号的产品");
        List<Integer> ids = entitys.stream().map(c->c.getId()).collect(Collectors.toList());
        ids.remove(dto.getId());
        Assert.assertFalse(!CollectionUtils.isEmpty(ids), "您的产品库中已存在相同编号的产品");
    }

}
