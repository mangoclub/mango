package com.mango.service.impl;


import com.google.common.collect.Lists;
import com.mango.dao.ClientDao;
import com.mango.dto.ClientDto;
import com.mango.dto.ClientWebDto;
import com.mango.dto.LabelValueDto;
import com.mango.dto.QueryCondition;
import com.mango.entity.Client;
import com.mango.enums.DailyRecordTypeEnum;
import com.mango.framework.BeanMappingService;
import com.mango.service.AbstractBasicService;
import com.mango.service.ClientService;
import com.mango.service.DailyRecordService;
import com.mango.utils.ApplicationException;
import com.mango.utils.EntityUtils;
import lombok.extern.log4j.Log4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;


@Service
@Log4j
public class ClientServiceImpl extends AbstractBasicService<ClientDto> implements ClientService {


    @Autowired
    ClientDao clientDao;
    @Autowired
    private BeanMappingService beanMappingService;
    @Autowired
    private DailyRecordService dailyRecordService;

    @Override
    public int create(ClientDto clientDto) {
        try {
            ClientDto.checkAndTransfer(clientDto);
            Client client = beanMappingService.transform(clientDto, Client.class);
            EntityUtils.init(client);
            clientDao.create(client);

            dailyRecordService.create(DailyRecordTypeEnum.CREATE_CLIENT_S.getCode(), DailyRecordTypeEnum.CREATE_CLIENT_S.getDesc());
            return client.getId();
        }catch (Exception e) {
            dailyRecordService.create(DailyRecordTypeEnum.CREATE_CLIENT_E.getCode(), StringUtils.join(Lists.newArrayList(DailyRecordTypeEnum.CREATE_CLIENT_E.getDesc(), e.getMessage()), ","));
            throw e;
        }

    }

    @Override
    public int update(ClientDto clientDto) {
        try {
            ClientDto.checkAndTransfer(clientDto);
            Client client = beanMappingService.transform(clientDto, Client.class);
            EntityUtils.update(client);
            clientDao.update(client);

            dailyRecordService.create(DailyRecordTypeEnum.UPDATE_CLIENT_S.getCode(), DailyRecordTypeEnum.UPDATE_CLIENT_S.getDesc());
            return client.getId();
        }catch (Exception e) {
            dailyRecordService.create(DailyRecordTypeEnum.UPDATE_CLIENT_E.getCode(), StringUtils.join(Lists.newArrayList(DailyRecordTypeEnum.UPDATE_CLIENT_E.getDesc(), e.getMessage()), ","));
            throw e;
        }
    }


    @Override
    public ClientDto findById(Integer id) {
        return beanMappingService.transform(clientDao.findById(id), ClientDto.class);
    }

    @Override
    public List<ClientDto> queryByCondition(QueryCondition queryCondition) {
        try {
            QueryCondition.checkAndTransfer(queryCondition);
            List<Client> clients = clientDao.queryByCondition(queryCondition);
            List<ClientDto> clientDtos = Lists.newArrayList();
            if(!CollectionUtils.isEmpty(clients)) {
               // clientDtos = Lists.transform(clients, client -> beanMappingService.transform(client, ClientDto.class));
                for(Client client : clients) {
                    ClientDto clientDto = beanMappingService.transform(client, ClientDto.class);
                    ClientDto.convert(clientDto);
                    clientDtos.add(clientDto);
                }
            }

            return clientDtos;
        }catch (Exception e) {
            throw e;
        }
    }

    @Override
    public int queryCountByCondition(QueryCondition queryCondition) {
        try {
            QueryCondition.checkAndTransfer(queryCondition);
            int total = clientDao.queryCountByCondition(queryCondition);

            return total;
        }catch (Exception e) {
            throw e;
        }
    }

    @Override
    public ClientWebDto query(QueryCondition queryCondition) {
        try {
            ClientWebDto clientWebDto = ClientWebDto.builder()
                    .total(queryCountByCondition(queryCondition))
                    .list(queryByCondition(queryCondition))
                    .build();
            dailyRecordService.create(DailyRecordTypeEnum.QUERY_CLIENT_S.getCode(), DailyRecordTypeEnum.QUERY_CLIENT_S.getDesc());
            return clientWebDto;
        } catch (Exception e) {
            dailyRecordService.create(DailyRecordTypeEnum.QUERY_CLIENT_E.getCode(), StringUtils.join(Lists.newArrayList(DailyRecordTypeEnum.QUERY_CLIENT_E.getDesc(), e.getMessage()), ","));
            throw e;
        }
    }


}
