package com.mango.service.impl;

import com.mango.constant.Constants;
import com.mango.enums.LockTypeEnum;
import com.mango.exception.LockFailException;
import com.mango.service.DistributedLockService;
import com.mango.service.impl.distributedLock.DistributedLockHandler;
import com.mango.utils.ApplicationException;
import lombok.extern.log4j.Log4j;
import org.joda.time.DateTime;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;

@Component
@Log4j
public class DistributedLockServiceImpl implements DistributedLockService {


    @Resource(name = "mySqlDistributedLockHandler")
    DistributedLockHandler mySqlDistributedLockHandler;

    @Override
    public Boolean lock(Integer lockType, Integer group, String key) {

        try {
            DistributedLockHandler handler = getHandler(lockType);
            handler.lock(generateKey(group, key), lockType, generateExpiredTime());
            return Boolean.TRUE;
        } catch (Exception e) {
            log.error(e.getMessage());
            throw e;
//            return Boolean.FALSE;
        }
    }

    @Override
    public Boolean unLock(Integer lockType, Integer group, String key) {
        try {
            DistributedLockHandler handler = getHandler(lockType);

            handler.unlock(generateKey(group, key), lockType);
            return Boolean.TRUE;
        } catch (Exception e) {
            log.error(e.getMessage());
            throw e;
//            return Boolean.FALSE;
        }
    }

    private Date generateExpiredTime() {
        return DateTime.now().plusMinutes(5).toDate();
    }

    private String generateKey(Integer group, String key) {
        return group + "_" + key;
    }


    private DistributedLockHandler getHandler(Integer lockType) {

        if (lockType.equals(LockTypeEnum.MYSQL.getCode())) {
            return mySqlDistributedLockHandler;
        }
        throw new LockFailException(Constants.FAIL_GET_LOCK + String.format("未获取到类型[%s]对应的实现", lockType));
    }


}
