package com.mango.service.impl;


import com.google.common.collect.Lists;
import com.mango.context.ContextContainer;
import com.mango.context.UserContext;
import com.mango.dao.DailyRecordDao;
import com.mango.dto.DailyRecordDto;
import com.mango.dto.DailyRecordWebDto;
import com.mango.dto.QueryCondition;
import com.mango.entity.DailyRecord;
import com.mango.enums.DailyRecordTypeEnum;
import com.mango.framework.BeanMappingService;
import com.mango.service.DailyRecordService;
import com.mango.utils.ApplicationException;
import com.mango.utils.EntityUtils;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Service
@Log4j
@Transactional
public class DailyRecordServiceImpl implements DailyRecordService {


    @Autowired
    DailyRecordDao dao;
    @Autowired
    private BeanMappingService beanMappingService;

    @Override
    public int create(DailyRecordDto dailyRecordDto) {
        try {
            UserContext userContext = ContextContainer.getUserContext();
            DailyRecord dailyRecord = beanMappingService.transform(dailyRecordDto, DailyRecord.class);
            EntityUtils.init(dailyRecord);
            dailyRecord.setDirection(checkDirection(dailyRecord.getDirection()));
            if(userContext != null) {
                if(userContext.getIp() != null) {
                    dailyRecord.setIp(userContext.getIp());
                }
                dailyRecord.setUserAgent(userContext.getUserAgent());
            }
            return dao.create(dailyRecord);
        }catch (Exception e) {
            throw e;
        }

    }

    @Override
    public int create(Integer type, String direction) {
        UserContext userContext = ContextContainer.getUserContext();
        DailyRecord dailyRecord = DailyRecord.builder()
                .accountName(userContext == null ? "" : userContext.getAccountName())
                .type(type)
                .direction(checkDirection(direction))
                .build();
        if(userContext != null) {
            if(userContext.getIp() != null) {
                dailyRecord.setIp(userContext.getIp());
            }
            dailyRecord.setUserAgent(userContext.getUserAgent());
        }
        EntityUtils.init(dailyRecord);
        return dao.create(dailyRecord);
    }

    @Override
    public int create(String ip,Integer type, String direction) {
        UserContext userContext = ContextContainer.getUserContext();
        DailyRecord dailyRecord = DailyRecord.builder()
                .accountName(userContext == null ? "" : userContext.getAccountName())
                .type(type)
                .direction(checkDirection(direction))
                .build();
        dailyRecord.setIp(ip);
        if(userContext != null) {
            dailyRecord.setUserAgent(userContext.getUserAgent());
        }
        EntityUtils.init(dailyRecord);
        return dao.create(dailyRecord);
    }
//
//    private void sendEmil(DailyRecord dailyRecord, String msg) {
//        if(dailyRecord.getType()%2 != 0) {
//            MutliThread thread = new MutliThread(dailyRecord.getAccountName()+":"+msg);
//            thread.start();
//        }
//    }
//    class MutliThread extends Thread{
//        private String msg;
//        MutliThread(String msg){
//            this.msg = msg;
//        }
//        public void run(){
//            log.error(msg);
//        }
//    }

    private String checkDirection(String direction) {
        return direction.length() > 250 ? direction.substring(0,249) : direction;
    }

    @Override
    public List<DailyRecordDto> queryByCondition(QueryCondition queryCondition) {
        try {
            QueryCondition.checkAndTransfer(queryCondition);
            List<DailyRecord> entitys = dao.queryByCondition(queryCondition);
            List<DailyRecordDto> dtos = Lists.newArrayList();
            if(!CollectionUtils.isEmpty(entitys)) {
                for(DailyRecord entity : entitys) {
                    DailyRecordDto dto = beanMappingService.transform(entity, DailyRecordDto.class);
                    dto.setIp(entity.getIp());
                    DailyRecordDto.convert(dto);
                    dtos.add(dto);
                }
            }

            return dtos;
        }catch (Exception e) {
            throw e;
        }
    }

    @Override
    public int queryCountByCondition(QueryCondition queryCondition) {
        try {
            QueryCondition.checkAndTransfer(queryCondition);
            int total = dao.queryCountByCondition(queryCondition);

            return total;
        }catch (Exception e) {
            throw e;
        }
    }

    @Override
    public DailyRecordWebDto query(QueryCondition queryCondition) {
        try {
            DailyRecordWebDto webDto = DailyRecordWebDto.builder()
                    .total(queryCountByCondition(queryCondition))
                    .list(queryByCondition(queryCondition))
                    .build();
            return webDto;
        } catch (Exception e) {
            throw e;
        }
    }


}
