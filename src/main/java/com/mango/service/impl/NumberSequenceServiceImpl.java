package com.mango.service.impl;

import com.mango.dao.NumberSequenceDao;
import com.mango.entity.NumberSequence;
import com.mango.enums.GroupTypeEnum;
import com.mango.enums.LockTypeEnum;
import com.mango.service.DistributedLockService;
import com.mango.service.NumberSequenceService;
import com.mango.utils.ApplicationException;
import com.mango.utils.NumberUtil;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;


@Component
@Log4j
public class NumberSequenceServiceImpl implements NumberSequenceService {


    @Autowired
    private NumberSequenceDao numberSequenceDao;
    @Autowired
    private DistributedLockService distributedLockService;
    private static final String EXPRESS = "%s_%s_%s";

    @Override
    public String getNumberSequenceByShopIdAndType(int shopId, int type, String typeName) {

        NumberSequence numberSequence = numberSequenceDao.getNumberByShopIdAndType(shopId, type);

        String key = String.format(EXPRESS,numberSequence == null ? "0" : numberSequence.getValue(), shopId, type);
        Boolean lock = null;
        try {
            String maxNumberSequence = "0";
            lock = distributedLockService.lock(LockTypeEnum.MYSQL.getCode(), GroupTypeEnum.GET_NUMBER.getCode(), key);
            if(numberSequence == null) {
                NumberSequence numberSeq = NumberSequence.builder().shopId(shopId).type(type).value("1").build();
                numberSequenceDao.create(numberSeq);
            }else {
                maxNumberSequence = numberSequence.getValue();
                String nextSequence = String.valueOf(Long.parseLong(maxNumberSequence) + 1);
                NumberSequence numberSeq = NumberSequence.builder().value(nextSequence).build();
                numberSeq.setId(numberSequence.getId());
                numberSeq.setUpdateTime(new Date());
                numberSequenceDao.update(numberSeq);
            }
            return getCode(typeName, maxNumberSequence);
        } catch (Exception e) {
            throw e;
        } finally {
            if(lock!=null && lock){
                distributedLockService.unLock(LockTypeEnum.MYSQL.getCode(), GroupTypeEnum.GET_NUMBER.getCode(), key);
            }
        }
    }


    public String getCode(String type, String maxSequence) {
        String resultString = NumberUtil.format(Long.parseLong(maxSequence) + 1, 4);
        resultString = type+"-"+resultString;
        return resultString;
    }

}
