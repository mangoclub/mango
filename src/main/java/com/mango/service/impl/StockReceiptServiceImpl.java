package com.mango.service.impl;


import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.mango.constant.Constants;
import com.mango.context.ContextContainer;
import com.mango.context.UserContext;
import com.mango.dao.InventoryDao;
import com.mango.dao.StockReceiptDao;
import com.mango.dto.*;
import com.mango.entity.Inventory;
import com.mango.entity.InventoryChangeFlow;
import com.mango.entity.StockReceipt;
import com.mango.entity.StockReceiptSku;
import com.mango.enums.DailyRecordTypeEnum;
import com.mango.enums.GroupTypeEnum;
import com.mango.enums.LockTypeEnum;
import com.mango.enums.NumberSequenceTypeEnum;
import com.mango.framework.BeanMappingService;
import com.mango.framework.Beans;
import com.mango.service.*;
import com.mango.utils.*;
import com.sun.org.apache.xpath.internal.operations.Bool;
import lombok.extern.log4j.Log4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Log4j
@Transactional(rollbackFor=Exception.class)
public class StockReceiptServiceImpl implements StockReceiptService {


    @Autowired
    StockReceiptDao dao;
    @Autowired
    InventoryService inventoryService;
    @Autowired
    InventoryChangeFlowService inventoryChangeFlowService;
    @Autowired
    private BeanMappingService beanMappingService;
    @Autowired
    private DailyRecordService dailyRecordService;
    @Autowired
    private NumberSequenceService numberSequenceService;
    @Autowired
    private StockReceiptSkuService stockReceiptSkuService;
    @Autowired
    private DistributedLockService distributedLockService;

    private String generateSerial(StockReceiptDto dto) {
        UserContext userContext = ContextContainer.getUserContext();
        String serial = numberSequenceService.getNumberSequenceByShopIdAndType(userContext.getShopId(),dto.getReceiptType(), dto.getTypeName());
        return serial;
    }

    @Override
    public int create(StockReceiptDto dto) {
        try {
            StockReceiptDto.checkAndTransfer(dto);

            dto.setSerial(generateSerial(dto));
            StockReceipt entity = beanMappingService.transform(dto, StockReceipt.class);
            EntityUtils.init(entity);
            dao.create(entity);
            dailyRecordService.create(DailyRecordTypeEnum.CREATE_RECEIPT_S.getCode(), DailyRecordTypeEnum.CREATE_RECEIPT_S.getDesc());
            return entity.getId();
        }catch (Exception e) {
            dailyRecordService.create(DailyRecordTypeEnum.CREATE_RECEIPT_E.getCode(), StringUtils.join(Lists.newArrayList(DailyRecordTypeEnum.CREATE_RECEIPT_E.getDesc(), e.getMessage()), ","));
            throw e;
        }

    }

    @Override
    public int update(StockReceiptDto dto) {
        try {
            StockReceiptDto.checkAndTransfer(dto);
            checkPermission(dto.getId(), true);
            StockReceipt entity = beanMappingService.transform(dto, StockReceipt.class);
            EntityUtils.update(entity);
            dao.update(entity);

            dailyRecordService.create(DailyRecordTypeEnum.UPDATE_RECEIPT_S.getCode(), DailyRecordTypeEnum.UPDATE_RECEIPT_S.getDesc());
            return entity.getId();
        }catch (Exception e) {
            dailyRecordService.create(DailyRecordTypeEnum.UPDATE_RECEIPT_E.getCode(), StringUtils.join(Lists.newArrayList(DailyRecordTypeEnum.UPDATE_RECEIPT_E.getDesc(), e.getMessage()), ","));
            throw e;
        }
    }


    @Override
    public StockReceiptDto findById(Integer id) {
        StockReceiptDto dto = beanMappingService.transform(dao.findById(id), StockReceiptDto.class);
        StockReceiptDto.convert(dto);
        List<StockReceiptSkuDto> skuDtos = stockReceiptSkuService.queryList(QueryCondition.builder().receiptId(id).build());
        if(CollectionUtils.isEmpty(skuDtos)) skuDtos = Lists.newArrayList();
        dto.setList(skuDtos);
        return dto;
    }

    @Override
    public List<StockReceiptDto> queryByCondition(QueryCondition queryCondition) {
        try {
            QueryCondition.checkAndTransfer(queryCondition);
            List<StockReceipt> entitys = dao.queryByCondition(queryCondition);
            List<StockReceiptDto> dtos = Lists.newArrayList();
            if(!CollectionUtils.isEmpty(entitys)) {
                for(StockReceipt entity : entitys) {
                    StockReceiptDto dto = beanMappingService.transform(entity, StockReceiptDto.class);
                    StockReceiptDto.convert(dto);
                    dtos.add(dto);
                }
            }

            return dtos;
        }catch (Exception e) {
            throw e;
        }
    }

    @Override
    public int queryCountByCondition(QueryCondition queryCondition) {
        try {
            QueryCondition.checkAndTransfer(queryCondition);
            int total = dao.queryCountByCondition(queryCondition);

            return total;
        }catch (Exception e) {
            throw e;
        }
    }

    @Override
    public StockReceiptWebDto query(QueryCondition queryCondition) {
        try {
            StockReceiptWebDto webDto = StockReceiptWebDto.builder()
                    .total(queryCountByCondition(queryCondition))
                    .list(queryByCondition(queryCondition))
                    .build();
            dailyRecordService.create(DailyRecordTypeEnum.QUERY_RECEIPT_S.getCode(), DailyRecordTypeEnum.QUERY_RECEIPT_S.getDesc());
            return webDto;
        } catch (Exception e) {
            dailyRecordService.create(DailyRecordTypeEnum.QUERY_RECEIPT_E.getCode(), StringUtils.join(Lists.newArrayList(DailyRecordTypeEnum.QUERY_RECEIPT_E.getDesc(), e.getMessage()), ","));
            throw e;
        }
    }

    @Override
    public boolean inventoryChange(InventoryChangeDto changeDto) {
        Boolean lock = null;

        Integer shopId = changeDto.getShopId();
        String lockKey =  String.valueOf(shopId);
        try {
            lock = distributedLockService.lock(LockTypeEnum.MYSQL.getCode(), GroupTypeEnum.INVENTORY_CHANGE.getCode(), lockKey);

            InventoryChangeDto.checkAndTransfer(changeDto);

            String operation = NumberSequenceTypeEnum.getOperationByCode(changeDto.getType());

            List<StockReceiptSkuDto> skuDtoList = changeDto.getSkuDtoList();

            //第一步，全部找出这些门店和产品的库存

            List<Integer> productIdList = skuDtoList.stream().map(c->c.getProductId()).collect(Collectors.toList());
            List<InventoryDto> inventories = inventoryService.queryInventory(shopId, productIdList);

            Map<String, InventoryDto> inventoryMap = inventories.stream().collect(Collectors.toMap(key->key.getShopId()+":"+key.getProductId(), value->value));
            Map<String, InventoryDto> oldInventoryMap = inventories.stream().collect(Collectors.toMap(key->key.getShopId()+":"+key.getProductId(), value->value));

            if(inventoryMap == null) inventoryMap = Maps.newHashMap();
            if(oldInventoryMap == null) oldInventoryMap = Maps.newHashMap();

            //第二步，加减库存，写明细

            for(StockReceiptSkuDto c : skuDtoList) {
                Assert.assertFalse(c.getCount() == null, c.getProductName()+",数量>0！");
                Assert.assertFalse(c.getCount() <= 0, c.getProductName()+",数量>0！");
                Assert.assertFalse(c.getPrice() == null, c.getProductName()+",价格>=0！");
                Assert.assertFalse(c.getPrice().compareTo(BigDecimal.ZERO) == -1, c.getProductName()+",价格>=0！");
                InventoryDto oldInventory = new InventoryDto();
                InventoryDto inventoryDto = new InventoryDto();
                if(inventoryMap != null && inventoryMap.get(shopId+":"+c.getProductId()) != null) {
                    oldInventory = oldInventoryMap.get(shopId+":"+c.getProductId());
                    inventoryDto = inventoryMap.get(shopId+":"+c.getProductId());
                    inventoryDto.setStockInventory(OperationUtil.operation(inventoryDto.getStockInventory(), operation, c.getCount()));
                    inventoryDto.setStockAvailable(OperationUtil.operation(inventoryDto.getStockAvailable(), operation, c.getCount()));
                    inventoryDto.setStockFrozen(inventoryDto.getStockInventory() - inventoryDto.getStockAvailable());

                    Assert.assertFalse(inventoryDto.getStockInventory() < 0, c.getProductName()+",库存不足！");

//                    BigDecimal cost = new BigDecimal(c.getCount()).multiply(c.getPrice());
//                    if(operation.equals(Constants.REDUCE)) {
//                        cost = new BigDecimal(c.getCount()).multiply(inventoryDto.getCostPrice());
//                    }
//                    BigDecimal totalCost = inventoryDto.getStockInventory() == 0 ? BigDecimal.ZERO : OperationUtil.operation(inventoryDto.getTotalCost(), operation, cost);
//                    BigDecimal costPrice = inventoryDto.getCostPrice();
//                    inventoryDto.setTotalCost(totalCost.compareTo(BigDecimal.ZERO) == 1 ? totalCost : BigDecimal.ZERO);
//
//                    if(!operation.equals(Constants.REDUCE)) {
//                        costPrice = inventoryDto.getTotalCost().divide(new BigDecimal(inventoryDto.getStockInventory()), 2, BigDecimal.ROUND_HALF_UP);
//                    }
//
//                    inventoryDto.setCostPrice(costPrice);
//                    if(operation.equals(Constants.REDUCE)) {
//                        inventoryDto.setTotalCost(costPrice.multiply(new BigDecimal(inventoryDto.getStockInventory())));
//                    }
                    BigDecimal cost;
                    BigDecimal totalCost;
                    BigDecimal costPrice;

                    if(operation.equals(Constants.ADD)) {
                        cost = new BigDecimal(c.getCount()).multiply(c.getPrice());
                        totalCost = OperationUtil.operation(inventoryDto.getTotalCost(), operation, cost);
                        costPrice = totalCost.divide(new BigDecimal(inventoryDto.getStockInventory()), 2, BigDecimal.ROUND_HALF_UP);
                    }else {
                        costPrice = inventoryDto.getCostPrice();
                        totalCost = costPrice.multiply(new BigDecimal(inventoryDto.getStockInventory()));
                    }
                    inventoryDto.setCostPrice(costPrice);
                    inventoryDto.setTotalCost(totalCost.compareTo(BigDecimal.ZERO) == 1 ? totalCost : BigDecimal.ZERO);

                    inventoryService.update(inventoryDto);
                }else if(operation.equals(Constants.ADD)){
                    inventoryDto.setShopId(shopId);
                    inventoryDto.setProductId(c.getProductId());
                    inventoryDto.setStockInventory(c.getCount());
                    inventoryDto.setStockAvailable(c.getCount());
                    inventoryDto.setStockFrozen(0);
                    inventoryDto.setCostPrice(c.getPrice());
                    inventoryDto.setTotalCost(c.getPrice().multiply(new BigDecimal(c.getCount())));

                    inventoryDto.setId(inventoryService.create(inventoryDto));
                }else {
                    Assert.assertFalse(true, c.getProductName()+",库存不足！");
                }

                changeFlow(changeDto, c, oldInventory, inventoryDto, operation);
                inventoryMap.put(shopId+":"+c.getProductId(), inventoryDto);
                oldInventory.setStockInventory(inventoryDto.getStockInventory());
                oldInventory.setCostPrice(inventoryDto.getCostPrice());
                oldInventoryMap.put(shopId+":"+c.getProductId(), oldInventory);
            }

            return true;
        } catch (Exception e) {
            throw e;
        } finally {
            if(lock!=null && lock){
                distributedLockService.unLock(LockTypeEnum.MYSQL.getCode(), GroupTypeEnum.INVENTORY_CHANGE.getCode(), lockKey);
            }
        }
    }


    private boolean changeFlow(InventoryChangeDto changeDto, StockReceiptSkuDto skuDto, InventoryDto oldInventory, InventoryDto inventoryDto, String operation) {
        InventoryChangeFlowDto changeFlowDto = InventoryChangeFlowDto.builder()
                .changeShopId(changeDto.getShopId())
                .type(changeDto.getType())
                .receiptId(changeDto.getReceiptId())
                .productId(skuDto.getProductId())
                .beforeInventory(oldInventory.getStockInventory())
                .afterInventory(inventoryDto.getStockInventory())
                .beforeCostPrice(oldInventory.getCostPrice())
                .afterCostPrice(inventoryDto.getCostPrice())
                .totalCost(inventoryDto.getTotalCost())
                .build();

        if(operation.equals(Constants.ADD)) {
            changeFlowDto.setInCount(skuDto.getCount());
            changeFlowDto.setInPrice(skuDto.getPrice());
            changeFlowDto.setInMoney(new BigDecimal(skuDto.getCount()).multiply(skuDto.getPrice()));
        }else if(operation.equals(Constants.REDUCE)) {
            changeFlowDto.setOutCount(skuDto.getCount());
            changeFlowDto.setOutPrice(skuDto.getPrice());
            changeFlowDto.setOutMoney(new BigDecimal(skuDto.getCount()).multiply(skuDto.getPrice()));
            changeFlowDto.setGainMoney(new BigDecimal(skuDto.getCount()).multiply((skuDto.getPrice().subtract(oldInventory.getCostPrice()))));
        }

        inventoryChangeFlowService.create(changeFlowDto);

        return true;
    }

    @Override
    public void checkPermission(Integer receiptId, Boolean canEdit) {
        StockReceiptDto receiptDto = findById(receiptId);
        Assert.assertFalse(receiptDto == null, "单据信息有误！");
        if(!canEdit) {
            Assert.assertFalse(receiptDto.getStatus() != 1, "单据不可更改,请核对单据状态！");
        }
    }
}
