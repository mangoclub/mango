package com.mango.service.impl;


import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.mango.context.ContextContainer;
import com.mango.dao.PayDao;
import com.mango.dto.*;
import com.mango.entity.Pay;
import com.mango.enums.DailyRecordTypeEnum;
import com.mango.enums.PayStatusEnum;
import com.mango.framework.BeanMappingService;
import com.mango.service.AbstractBasicService;
import com.mango.service.PayService;
import com.mango.service.DailyRecordService;
import com.mango.service.ShopService;
import com.mango.utils.ApplicationException;
import com.mango.utils.DateUtils;
import com.mango.utils.EntityUtils;
import lombok.extern.log4j.Log4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.List;


@Service
@Log4j
public class PayServiceImpl extends AbstractBasicService<PayDto> implements PayService {


    @Autowired
    PayDao dao;
    @Autowired
    private BeanMappingService beanMappingService;
    @Autowired
    private DailyRecordService dailyRecordService;
    @Autowired
    private ShopService shopService;

    @Override
    public int create(PayDto dto) {
        try {
            PayDto.checkAndTransfer(dto);
            Pay entity = beanMappingService.transform(dto, Pay.class);
            entity.setStatus(PayStatusEnum.WAIT.getCode());
            EntityUtils.init(entity);
            if(ContextContainer.getUserContext() != null) {
                ShopDto shopDto = shopService.findById(ContextContainer.getUserContext().getShopId());
                Date endDate = Strings.isNullOrEmpty(shopDto.getExtend3()) ? new Date() :
                        (DateUtils.getDateFormat(shopDto.getExtend3()).compareTo(new Date()) >= 0 ? DateUtils.getDateFormat(shopDto.getExtend3()) : new Date());
                entity.setExpiredDate(DateUtils.formatSecond(endDate));
                entity.setExtendDate(DateUtils.formatSecond(DateUtils.getBackDate(endDate, entity.getExtendMonth())));
            }
            dao.create(entity);

            dailyRecordService.create(DailyRecordTypeEnum.CREATE_PAY_S.getCode(), DailyRecordTypeEnum.CREATE_PAY_S.getDesc());
            return entity.getId();
        }catch (Exception e) {
            dailyRecordService.create(DailyRecordTypeEnum.CREATE_PAY_E.getCode(), StringUtils.join(Lists.newArrayList(DailyRecordTypeEnum.CREATE_PAY_E.getDesc(), e.getMessage()), ","));
            throw e;
        }

    }

    @Override
    public int update(PayDto dto) {
        try {
            PayDto.checkAndTransfer(dto);
            Pay entity = beanMappingService.transform(dto, Pay.class);
            EntityUtils.update(entity);
            dao.update(entity);

            dailyRecordService.create(DailyRecordTypeEnum.UPDATE_PAY_S.getCode(), DailyRecordTypeEnum.UPDATE_PAY_S.getDesc());
            return entity.getId();
        }catch (Exception e) {
            dailyRecordService.create(DailyRecordTypeEnum.UPDATE_PAY_E.getCode(), StringUtils.join(Lists.newArrayList(DailyRecordTypeEnum.UPDATE_PAY_E.getDesc(), e.getMessage()), ","));
            throw e;
        }
    }

    @Override
    public int cofirm(PayDto dto) {
        try {
            PayDto.checkAndTransfer(dto);
            Pay entity = beanMappingService.transform(dto, Pay.class);
            entity.setStatus(PayStatusEnum.CONFIRED.getCode());
            ShopDto shopDto = shopService.findById(entity.getShopId());
            Date endDate = Strings.isNullOrEmpty(shopDto.getExtend3()) ? new Date() :
                    (DateUtils.getDateFormat(shopDto.getExtend3()).compareTo(new Date()) >= 0 ? DateUtils.getDateFormat(shopDto.getExtend3()) : new Date());
            String extendDate = DateUtils.formatSecond(DateUtils.getBackDate(endDate, entity.getExtendMonth()));
            entity.setExtendDate(extendDate);
            EntityUtils.update(entity);
            dao.update(entity);

            shopDto.setExtend3(extendDate);
            shopService.extend(shopDto);

            dailyRecordService.create(DailyRecordTypeEnum.CONFIRM_PAY_S.getCode(), DailyRecordTypeEnum.CONFIRM_PAY_S.getDesc());
            return entity.getId();
        }catch (Exception e) {
            dailyRecordService.create(DailyRecordTypeEnum.CONFIRM_PAY_E.getCode(), StringUtils.join(Lists.newArrayList(DailyRecordTypeEnum.CONFIRM_PAY_E.getDesc(), e.getMessage()), ","));
            throw e;
        }
    }

    @Override
    public int delete(PayDto dto) {
        try {
            PayDto.checkAndTransfer(dto);
            Pay entity = beanMappingService.transform(dto, Pay.class);
            entity.setStatus(PayStatusEnum.DELETED.getCode());
            EntityUtils.update(entity);
            dao.changeStatus(entity);

            dailyRecordService.create(DailyRecordTypeEnum.DELETE_PAY_S.getCode(), DailyRecordTypeEnum.DELETE_PAY_S.getDesc());
            return entity.getId();
        }catch (Exception e) {
            dailyRecordService.create(DailyRecordTypeEnum.DELETE_PAY_E.getCode(), StringUtils.join(Lists.newArrayList(DailyRecordTypeEnum.DELETE_PAY_E.getDesc(), e.getMessage()), ","));
            throw e;
        }
    }


    @Override
    public PayDto findById(Integer id) {
        return beanMappingService.transform(dao.findById(id), PayDto.class);
    }

    @Override
    public List<PayDto> queryByCondition(QueryCondition queryCondition) {
        try {
            QueryCondition.checkAndTransfer(queryCondition);
            List<Pay> entitys = dao.queryByCondition(queryCondition);
            List<PayDto> dtos = Lists.newArrayList();
            if(!CollectionUtils.isEmpty(entitys)) {
                for(Pay entity : entitys) {
                    PayDto dto = beanMappingService.transform(entity, PayDto.class);
                    PayDto.convert(dto);
                    dtos.add(dto);
                }
            }

            return dtos;
        }catch (Exception e) {
            throw e;
        }
    }

    @Override
    public int queryCountByCondition(QueryCondition queryCondition) {
        try {
            QueryCondition.checkAndTransfer(queryCondition);
            int total = dao.queryCountByCondition(queryCondition);

            return total;
        }catch (Exception e) {
            throw e;
        }
    }

    @Override
    public PayWebDto query(QueryCondition queryCondition) {
        try {
            PayWebDto entityWebDto = PayWebDto.builder()
                    .total(queryCountByCondition(queryCondition))
                    .list(queryByCondition(queryCondition))
                    .build();
            dailyRecordService.create(DailyRecordTypeEnum.QUERY_PAY_S.getCode(), DailyRecordTypeEnum.QUERY_PAY_S.getDesc());
            return entityWebDto;
        } catch (Exception e) {
            dailyRecordService.create(DailyRecordTypeEnum.QUERY_PAY_E.getCode(), StringUtils.join(Lists.newArrayList(DailyRecordTypeEnum.QUERY_PAY_E.getDesc(), e.getMessage()), ","));
            throw e;
        }
    }


}
