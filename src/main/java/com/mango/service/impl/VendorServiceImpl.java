package com.mango.service.impl;


import com.google.common.collect.Lists;
import com.mango.dao.VendorDao;
import com.mango.dto.LabelValueDto;
import com.mango.dto.QueryCondition;
import com.mango.dto.VendorDto;
import com.mango.dto.VendorWebDto;
import com.mango.entity.Vendor;
import com.mango.enums.DailyRecordTypeEnum;
import com.mango.framework.BeanMappingService;
import com.mango.service.AbstractBasicService;
import com.mango.service.DailyRecordService;
import com.mango.service.VendorService;
import com.mango.utils.ApplicationException;
import com.mango.utils.EntityUtils;
import lombok.extern.log4j.Log4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Service
@Log4j
public class VendorServiceImpl extends AbstractBasicService<VendorDto> implements VendorService {


    @Autowired
    VendorDao vendorDao;
    @Autowired
    private BeanMappingService beanMappingService;
    @Autowired
    private DailyRecordService dailyRecordService;

    @Override
    public int create(VendorDto vendorDto) {
        try {
            VendorDto.checkAndTransfer(vendorDto);
            Vendor vendor = beanMappingService.transform(vendorDto, Vendor.class);
            EntityUtils.init(vendor);
            vendorDao.create(vendor);

            dailyRecordService.create(DailyRecordTypeEnum.CREATE_VENDOR_S.getCode(), DailyRecordTypeEnum.CREATE_VENDOR_S.getDesc());
            return vendor.getId();
        }catch (Exception e) {
            dailyRecordService.create(DailyRecordTypeEnum.CREATE_VENDOR_E.getCode(), StringUtils.join(Lists.newArrayList(DailyRecordTypeEnum.CREATE_VENDOR_E.getDesc(), e.getMessage()), ","));
            throw e;
        }

    }

    @Override
    public int update(VendorDto vendorDto) {
        try {
            VendorDto.checkAndTransfer(vendorDto);
            Vendor vendor = beanMappingService.transform(vendorDto, Vendor.class);
            EntityUtils.update(vendor);
            vendorDao.update(vendor);

            dailyRecordService.create(DailyRecordTypeEnum.UPDATE_VENDOR_S.getCode(), DailyRecordTypeEnum.UPDATE_VENDOR_S.getDesc());
            return vendor.getId();
        }catch (Exception e) {
            dailyRecordService.create(DailyRecordTypeEnum.UPDATE_VENDOR_E.getCode(), StringUtils.join(Lists.newArrayList(DailyRecordTypeEnum.UPDATE_VENDOR_E.getDesc(), e.getMessage()), ","));
            throw e;
        }
    }


    @Override
    public VendorDto findById(Integer id) {
        return beanMappingService.transform(vendorDao.findById(id), VendorDto.class);
    }

    @Override
    public List<VendorDto> queryByCondition(QueryCondition queryCondition) {
        try {
            QueryCondition.checkAndTransfer(queryCondition);
            List<Vendor> vendors= vendorDao.queryByCondition(queryCondition);
            List<VendorDto> vendorDtos = Lists.newArrayList();
            if(!CollectionUtils.isEmpty(vendors)) {
                for(Vendor vendor : vendors) {
                    VendorDto vendorDto = beanMappingService.transform(vendor, VendorDto.class);
                    VendorDto.convert(vendorDto);
                    vendorDtos.add(vendorDto);
                }
            }

            return vendorDtos;
        }catch (Exception e) {
            throw e;
        }
    }

    @Override
    public int queryCountByCondition(QueryCondition queryCondition) {
        try {
            QueryCondition.checkAndTransfer(queryCondition);
            int total = vendorDao.queryCountByCondition(queryCondition);

            return total;
        }catch (Exception e) {
            throw e;
        }
    }

    @Override
    public VendorWebDto query(QueryCondition queryCondition) {
        try {
            VendorWebDto vendorWebDto = VendorWebDto.builder()
                    .total(queryCountByCondition(queryCondition))
                    .list(queryByCondition(queryCondition))
                    .build();
            dailyRecordService.create(DailyRecordTypeEnum.QUERY_VENDOR_S.getCode(), DailyRecordTypeEnum.QUERY_VENDOR_S.getDesc());
            return vendorWebDto;
        } catch (Exception e) {
            dailyRecordService.create(DailyRecordTypeEnum.QUERY_VENDOR_E.getCode(), StringUtils.join(Lists.newArrayList(DailyRecordTypeEnum.QUERY_VENDOR_E.getDesc(), e.getMessage()), ","));
            throw e;
        }
    }


}
