package com.mango.service;


import com.mango.dto.ClientDto;
import com.mango.dto.ClientWebDto;
import com.mango.dto.LabelValueDto;
import com.mango.dto.QueryCondition;

import java.util.List;

public interface ClientService {

    int create(ClientDto clientDto);

    int update(ClientDto clientDto);

    ClientDto findById(Integer id);

    List<ClientDto> queryByCondition(QueryCondition queryCondition);

    int queryCountByCondition(QueryCondition queryCondition);

    ClientWebDto query(QueryCondition queryCondition);

    List<LabelValueDto> generateLabelValueList(List<ClientDto> list);

}