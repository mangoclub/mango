package com.mango.service;


import com.mango.dto.InventoryDto;
import com.mango.dto.InventoryWebDto;
import com.mango.dto.QueryCondition;

import java.util.List;


public interface InventoryService {

    int create(InventoryDto dto);

    int update(InventoryDto dto);

    InventoryDto findById(Integer id);

    List<InventoryDto> queryByCondition(QueryCondition queryCondition);

    int queryCountByCondition(QueryCondition queryCondition);

    InventoryWebDto query(QueryCondition queryCondition);

    List<InventoryDto> queryInventory(Integer shopId, List<Integer> productIds);

    InventoryDto queryInventoryByProductId(Integer shopId, Integer productId);


}