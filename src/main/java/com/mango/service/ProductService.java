package com.mango.service;


import com.mango.dto.LabelValueDto;
import com.mango.dto.ProductDto;
import com.mango.dto.ProductWebDto;
import com.mango.dto.QueryCondition;

import java.util.List;

public interface ProductService {

    int create(ProductDto dto);

    int update(ProductDto dto);

    ProductDto findById(Integer id);

    List<ProductDto> queryByCondition(QueryCondition queryCondition);

    int queryCountByCondition(QueryCondition queryCondition);

    ProductWebDto query(QueryCondition queryCondition);

    List<LabelValueDto> generateLabelValueList(List<ProductDto> list);

}