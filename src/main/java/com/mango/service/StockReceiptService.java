package com.mango.service;


import com.mango.dto.InventoryChangeDto;
import com.mango.dto.QueryCondition;
import com.mango.dto.StockReceiptDto;
import com.mango.dto.StockReceiptWebDto;

import java.util.List;

public interface StockReceiptService {

    int create(StockReceiptDto dto);

    int update(StockReceiptDto dto);

    StockReceiptDto findById(Integer id);

    List<StockReceiptDto> queryByCondition(QueryCondition queryCondition);

    int queryCountByCondition(QueryCondition queryCondition);

    StockReceiptWebDto query(QueryCondition queryCondition);

    boolean inventoryChange(InventoryChangeDto changeDto);

    void checkPermission(Integer receiptId, Boolean canEdit);
}