package com.mango.service;


import com.mango.context.UserContext;
import com.mango.dto.QueryCondition;
import com.mango.dto.UserDto;
import com.mango.dto.UserWebDto;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

public interface UserService {

    int create(UserDto dto);

    int update(UserDto dto);

    UserDto login(UserDto dto, HttpSession httpSession, UserContext userContext, HttpServletRequest httpServletRequest);

    boolean logout(HttpSession httpSession);

    boolean changePassword(UserDto dto, HttpSession httpSession);

    boolean resetPassword(UserDto dto, HttpSession httpSession);

    UserDto userInfo(HttpSession httpSession);

    UserDto findById(Integer id);

    List<UserDto> queryByConditionWithAnd(UserDto dto);

    void enrichUserInfo(Integer shopId, HttpSession httpSession, UserContext userContext, HttpServletRequest httpServletRequest);

    List<UserDto> queryByCondition(QueryCondition queryCondition);

    int queryCountByCondition(QueryCondition queryCondition);

    UserWebDto query(QueryCondition queryCondition);

}