package com.mango.service;


public interface NumberSequenceService {
    String getNumberSequenceByShopIdAndType(int customerId, int type, String typeDesc);
}
