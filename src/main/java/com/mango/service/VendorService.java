package com.mango.service;


import com.mango.dto.LabelValueDto;
import com.mango.dto.QueryCondition;
import com.mango.dto.VendorDto;
import com.mango.dto.VendorWebDto;

import java.util.List;

public interface VendorService {

    int create(VendorDto vendorDto);

    int update(VendorDto vendorDto);

    VendorDto findById(Integer id);

    List<VendorDto> queryByCondition(QueryCondition queryCondition);

    int queryCountByCondition(QueryCondition queryCondition);

    VendorWebDto query(QueryCondition queryCondition);

    List<LabelValueDto> generateLabelValueList(List<VendorDto> list);

}