package com.mango.service;


import com.mango.dto.NoticeDto;
import com.mango.dto.NoticeWebDto;
import com.mango.dto.QueryCondition;

import java.util.List;

public interface NoticeService {

    int create(NoticeDto dto);
    
    int update(NoticeDto dto);

    NoticeDto findById(Integer id);

    List<NoticeDto> queryByCondition(QueryCondition queryCondition);

    int queryCountByCondition(QueryCondition queryCondition);

    NoticeWebDto query(QueryCondition queryCondition);

    int publish(NoticeDto dto);

    int release(NoticeDto dto);

    NoticeDto findByStatus(Integer status);

}