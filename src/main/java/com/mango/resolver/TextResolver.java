package com.mango.resolver;


import com.google.gson.Gson;
import com.mango.dto.ResultMap;
import com.mango.utils.ApplicationException;
import lombok.extern.log4j.Log4j;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;

import javax.naming.NoPermissionException;
import javax.security.auth.login.LoginException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.Map;
import java.util.Objects;

@Component
@Log4j
public class TextResolver implements HandlerExceptionResolver {


    static class TextView implements View {
        private ResultMap msg;

        public TextView(ResultMap msg) {
            this.msg = msg;
        }

        @Override
        public String getContentType() {
            return "application/json";
        }

        @Override
        public void render(Map<String, ?> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
//            response.setContentType(getContentType());
            response.setCharacterEncoding("utf8");
            response.setContentType("text/plain; charset=utf-8");
            response.setStatus(200);
            PrintWriter writer = response.getWriter();
            writer.print(new Gson().toJson(this.msg));
            writer.flush();
        }
    }

    @Override
    public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {

        log.error(ExceptionUtils.getFullStackTrace(ex));
        if (ex instanceof ApplicationException || ex instanceof NoPermissionException) {
            return new ModelAndView(new TextView(ResultMap.builder().code(2003).msg(ex.getMessage()).build()));
        }
        if (ex instanceof LoginException) {

            String queryString = request.getQueryString();
            if (Objects.isNull(queryString)) {
                queryString = "";
            } else {
                queryString = "?" + queryString;
            }
            String redirectUrl = "redirect:/login?redirect_url=" + request.getRequestURL() + queryString;
            if(request.getRequestURL().toString().contains("/api")) {
                return new ModelAndView(new TextView(ResultMap.builder().code(2001).msg(ex.getMessage()).build()));
            }
            return new ModelAndView(redirectUrl);
        }

        return null;
    }


}
