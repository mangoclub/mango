package com.mango.constant;

public class Constants {
    public static final int  QUERY_LIMIT = 1000;
    public static final String BATCH_SEARCH_LIMIT_ERROR_INFO="超出批量查询限制:%d";
    public static final String FAIL_GET_LOCK = "请重新操作！";
    public static final String MIUNTE_FORMAT = "yyyy-MM-dd HH:mm";
    public static final int VERSION_INCREMENT = 1;

    public static final String ADD = "+";
    public static final String REDUCE = "-";

    public static final String DEFAULT_PASSWORD = "123456";
}
