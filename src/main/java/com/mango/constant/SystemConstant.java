/*
 *	Copyright © 2013 Changsha mango Network Technology Co., Ltd. All rights reserved.
 *	长沙市师说网络科技有限公司 版权所有
 *	http://www.mango.com
 */

package com.mango.constant;


/**
 * 系统常量
 * 
 */
public class SystemConstant {


	/**
	 * 上传文件夹
	 */
	public static String UPLOAD_FOLDER = "upload/photo";

	/**
	 * 备份文件夹
	 */
	public static String BACKUP_FOLDER = "/WEB-INF/backup";

	/**
	 * Session中的管理员Key
	 */
	public static final String SESSION_ADMIN = "SESSION_ADMIN";

	/**
	 * 头像URL 180x180
	 */
	public static final String FACE_URL = "http://faceurl.mango.com/face";

	/**
	 * 
	 */
	public static final String LANGUAGE = "language";

}
