package com.mango.context;


import com.mango.dto.ShopDto;
import com.mango.dto.UserDto;
import com.mango.service.ShopService;
import com.mango.service.UserService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.ExecutionException;


@Component
public class ContextHandler {

    @Autowired
    private ShopService shopService;
    @Autowired
    private UserService userService;

    public void enrichShopInfo(String userInfo) throws ExecutionException {
        String[] objects = StringUtils.split(userInfo,",");
        Integer shopId = Integer.valueOf((objects[0]));
        UserContext u = ContextContainer.getUserContext();
        enrichShopInfo(shopId, u);

    }


    private void enrichShopInfo(Integer shopId, UserContext u) {
        ShopDto shopDto = shopService.findById(shopId);
        if(shopDto!=null){
            u.setShopDto(shopDto);
            u.setShopId(shopId);
            u.setAccountName(shopDto.getAccountName());
        }
    }

    public void enrichUserInfo(String userInfo) throws ExecutionException {
        String[] objects = StringUtils.split(userInfo,",");
        Integer userId = Integer.valueOf((objects[0]));
        UserContext u = ContextContainer.getUserContext();
        enrichUserInfo(userId, objects[1], objects[2], u);

    }


    private void enrichUserInfo(Integer userId, String ip, String userAgent, UserContext u) {
        u.setIp(ip);
        u.setUserAgent(userAgent);
        UserDto userDto = userService.findById(userId);
        if(userDto!=null){
            ShopDto shopDto = shopService.findById(userDto.getShopId());
            if(shopDto!=null){
                u.setShopDto(shopDto);
                u.setShopId(shopDto.getId());
                u.setAccountName(shopDto.getAccountName());

                u.setUserDto(userDto);
                u.setUserId(userId);
            }
        }
    }


    public void clear(){
       // cache.clear();
    }


}
