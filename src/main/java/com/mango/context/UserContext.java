package com.mango.context;

import com.mango.dto.ShopDto;
import com.mango.dto.UserDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserContext {
    private ShopDto shopDto;
    private UserDto userDto;
    private int shopId;
    private int userId;
    private String accountName;
    private String imageUrl;
    private String languageCode="0001";
    private String ip;
    private String userAgent;

}
