package com.mango.framework;

public interface BeanMappingService {

    <T> T transform(Object o, Class<T> clazz);
}
