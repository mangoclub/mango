package com.mango.enums;

/**
 * Created by zaza on 16/1/7.
 */
public enum GroupTypeEnum {

    INVENTORY_CHANGE(1,"库存变化","inventoryChange"),
    COST(2,"计算入库成本价","costCal"),
    ORDER(3,"订单幂等处理","orderCal"),
    PROCESS(4,"流程流转","processExchange"),
    PROMOTION(5,"促销幂等处理","promotionCal"),
    RECEIPT(6,"单据幂等处理","stockReceipt"),
    SKU(7, "库存扣减", "skuStock"),
    VISIT(8,"生成会员回访任务","generateVisitTask"),
    SAP(9,"sap传送","sapPush"),
    GET_NUMBER(10,"取号","getNumber"),
    MEMBER_EXTRACT(11,"生成会员回访任务","generateVisitTask"),
    RECEIPT_IMPORT_SKU(12,"单据导入产品","importSku");



    private int code;
    private String expression;
    private String desc;

    private GroupTypeEnum(int code, String desc, String expression) {
        this.code = code;
        this.desc = desc;
        this.expression = expression;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }
}
