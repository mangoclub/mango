package com.mango.enums;


public enum LockTypeEnum {
    REDIS(1,"基于Redis的实现"),
    MEMCACHED(2,"基于Memcached的实现"),
    ZOOKEEPER(3,"基于Zookeeper的实现"),
    MYSQL(4,"基于MySQL的实现");

    private int code;
    private String desc;

    private LockTypeEnum(int code, String desc){
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
