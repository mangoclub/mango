package com.mango.enums;

public enum PayStatusEnum {

    WAIT(1,"待确认"),
    CONFIRED(2,"已确认"),
    DELETED(3,"已删除");

    private int code;
    private String desc;

    private PayStatusEnum(int code, String desc){
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public static String getDescByCode(int code) {
        String ret = null;
        for (PayStatusEnum enumItem : PayStatusEnum.values()) {
            if (enumItem.getCode() == code) {
                ret = enumItem.getDesc();
            }
        }
        return ret;
    }
}
