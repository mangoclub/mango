package com.mango.enums;


import com.mango.constant.Constants;


public enum NumberSequenceTypeEnum {

    RKD(1,"RKD", "入库单", Constants.ADD),
    CKD(2, "CKD","出库单", Constants.REDUCE);


    private Integer code;
    private String name;
    private String desc;
    private String operation;

    private NumberSequenceTypeEnum(Integer code, String name, String desc, String operation) {
        this.code = code;
        this.desc = desc;
        this.name = name;
        this.operation = operation;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public static String getDescByCode(Integer code) {
        String ret = null;
        for (NumberSequenceTypeEnum enumItem : NumberSequenceTypeEnum.values()) {
            if (enumItem.getCode().equals(code)) {
                ret = enumItem.getDesc();
            }
        }
        return ret;
    }

    public static String getOperationByCode(Integer code) {
        String ret = null;
        for (NumberSequenceTypeEnum enumItem : NumberSequenceTypeEnum.values()) {
            if (enumItem.getCode().equals(code)) {
                ret = enumItem.getOperation();
            }
        }
        return ret;
    }
}
