package com.mango.enums;

public enum NoticeStatusEnum {

    PUBLISH(1,"发布中"),
    RELEASE(2,"公告");

    private int code;
    private String desc;

    private NoticeStatusEnum(int code, String desc){
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public static String getDescByCode(int code) {
        String ret = null;
        for (NoticeStatusEnum enumItem : NoticeStatusEnum.values()) {
            if (enumItem.getCode() == code) {
                ret = enumItem.getDesc();
            }
        }
        return ret;
    }
}
