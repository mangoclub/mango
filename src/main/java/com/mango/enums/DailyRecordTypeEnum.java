package com.mango.enums;

public enum DailyRecordTypeEnum {
    //奇数失败，偶数成功
    REGISTER_S(0, "注册成功"),
    REGISTER_E(1, "error:注册失败"),

    CREATE_CLIENT_S(20, "新增客户成功"),
    CREATE_CLIENT_E(21, "error:新增客户失败"),
    UPDATE_CLIENT_S(22, "更新客户成功"),
    UPDATE_CLIENT_E(23, "error:更新客户失败"),
    QUERY_CLIENT_S(24, "搜索客户成功"),
    QUERY_CLIENT_E(25, "error:搜索客户失败"),

    CREATE_VENDOR_S(40, "新增供应商成功"),
    CREATE_VENDOR_E(41, "error:新增供应商失败"),
    UPDATE_VENDOR_S(42, "更新供应商成功"),
    UPDATE_VENDOR_E(43, "error:更新供应商失败"),
    QUERY_VENDOR_S(44, "搜索供应商成功"),
    QUERY_VENDOR_E(45, "error:搜索供应商失败"),

    CREATE_PRODUCT_S(60, "新增产品成功"),
    CREATE_PRODUCT_E(61, "error:新增产品失败"),
    UPDATE_PRODUCT_S(62, "更新产品成功"),
    UPDATE_PRODUCT_E(63, "error:更新产品失败"),
    QUERY_PRODUCT_S(64, "搜索产品成功"),
    QUERY_PRODUCT_E(65, "error:搜索产品失败"),

    CREATE_RECEIPT_S(80, "新增单据成功"),
    CREATE_RECEIPT_E(81, "error:新增单据失败"),
    UPDATE_RECEIPT_S(82, "更新单据成功"),
    UPDATE_RECEIPT_E(83, "error:更新单据失败"),
    QUERY_RECEIPT_S(84, "搜索单据成功"),
    QUERY_RECEIPT_E(85, "error:搜索单据失败"),
    DELETE_RECEIPT_S(86, "删除单据成功"),
    DELETE_RECEIPT_E(87, "error:删除单据失败"),

    CREATE_INVENTORY_S(100, "新增库存成功"),
    CREATE_INVENTORY_E(101, "error:新增库存失败"),
    UPDATE_INVENTORY_S(102, "更新库存成功"),
    UPDATE_INVENTORY_E(103, "error:更新库存失败"),
    QUERY_INVENTORY_S(104, "搜索库存成功"),
    QUERY_INVENTORY_E(105, "error:搜索库存失败"),

    CREATE_INVENTORY_FLOW_S(120, "新增库存明细成功"),
    CREATE_INVENTORY_FLOW_E(121, "error:新增库存明细失败"),
    UPDATE_INVENTORY_FLOW_S(122, "更新库存明细成功"),
    UPDATE_INVENTORY_FLOW_E(123, "error:更新库存明细失败"),
    QUERY_INVENTORY_FLOW_S(124, "搜索库存明细成功"),
    QUERY_INVENTORY_FLOW_E(125, "error:搜索库存明细失败"),
    QUERYCOLLECT_INVENTORY_FLOW_S(126, "搜索首页明细成功"),
    QUERYCOLLECT_INVENTORY_FLOW_E(127, "error:搜索首页明细失败"),

    CREATE_RECEIPT_SKU_S(140, "新增单据sku成功"),
    CREATE_RECEIPT_SKU_E(141, "error:新增单据sku失败"),
    UPDATE_RECEIPT_SKU_S(142, "更新单据sku成功"),
    UPDATE_RECEIPT_SKU_E(143, "error:更新单据sku失败"),
    QUERY_RECEIPT_SKU_S(144, "搜索单据sku成功"),
    QUERY_RECEIPT_SKU_E(145, "error:搜索单据sku失败"),
    DELETE_RECEIPT_SKU_S(146, "删除单据sku成功"),
    DELETE_RECEIPT_SKU_E(147, "error:删除单据sku失败"),

    CREATE_NOTICE_S(160, "新增公告成功"),
    CREATE_NOTICE_E(161, "error:新增公告失败"),
    UPDATE_NOTICE_S(162, "更新公告成功"),
    UPDATE_NOTICE_E(163, "error:更新公告失败"),
    QUERY_NOTICE_S(164, "搜索公告成功"),
    QUERY_NOTICE_E(165, "error:搜索公告失败"),
    PUBLISH_NOTICE_S(166, "发布公告成功"),
    PUBLISH_NOTICE_E(167, "error:发布公告失败"),
    RELEASE_NOTICE_S(168, "取消公告成功"),
    RELEASE_NOTICE_E(169, "error:取消公告失败"),

    CREATE_PAY_S(180, "新增续费成功"),
    CREATE_PAY_E(181, "error:新增续费失败"),
    UPDATE_PAY_S(182, "更新续费成功"),
    UPDATE_PAY_E(183, "error:更新续费失败"),
    QUERY_PAY_S(184, "搜索续费成功"),
    QUERY_PAY_E(185, "error:搜索续费失败"),
    CONFIRM_PAY_S(186, "确认续费成功"),
    CONFIRM_PAY_E(187, "error:确认续费失败"),
    DELETE_PAY_S(188, "删除续费成功"),
    DELETE_PAY_E(169, "error:删除续费失败"),


    TYPE_VISIT(500, "visit"),
    TYPE_REGISTER(502, "register"),
    TYPE_LOGIN(504, "login"),
    TYPE_EXCEPTION(506, "exception"),


    LOGIN_S(2, "登录成功"),
    LOGIN_E(3, "error:登录失败"),
    LOGOUT_S(4, "登出成功"),
    LOGOUT_E(5, "error:登出失败"),
    USERINFO_S(6, "个人信息获取成功"),
    USERINFO_E(7, "error:个人信息获取失败"),
    QUERY_USER_S(8, "搜索用户成功"),
    QUERY_USER_E(9, "error:搜索用户失败"),
    UPDATE_PASSWORD_S(10, "修改密码成功"),
    UPDATE_PASSWORD_E(11, "error:修改密码失败"),
    RESET_PASSWORD_S(12, "重置密码成功"),
    RESET_PASSWORD_E(13, "error:重置密码失败"),
    UPDATE_SHOP_S(14, "更新门店成功"),
    UPDATE_SHOP_E(15, "error:更新门店失败");


    private int code;
    private String desc;

    private DailyRecordTypeEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
