package com.mango.enums;

public enum ResultMapCodeEnum {
    SUCESS(1000, "操作成功"),
    FAILED(1001, "操作失败");


    private int code;
    private String desc;

    private ResultMapCodeEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
