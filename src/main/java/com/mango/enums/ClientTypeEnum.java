package com.mango.enums;

public enum ClientTypeEnum {

    NORMAL("1", "常客"),
    STRANGER("2", "散客");


    private String code;
    private String desc;

    private ClientTypeEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public static String getDescByCode(String code) {
        String ret = null;
        for (ClientTypeEnum enumItem : ClientTypeEnum.values()) {
            if (enumItem.getCode().equals(code)) {
                ret = enumItem.getDesc();
            }
        }
        return ret;
    }
}
