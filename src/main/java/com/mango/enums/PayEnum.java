package com.mango.enums;

public enum PayEnum {

    hasPay("1", "已经付款"),
    hasPayNot("2", "未付款");


    private String code;
    private String desc;

    private PayEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public static String getDescByCode(String code) {
        String ret = null;
        for (PayEnum enumItem : PayEnum.values()) {
            if (enumItem.getCode().equals(code)) {
                ret = enumItem.getDesc();
            }
        }
        return ret;
    }
}
