package com.mango.enums;

public enum StockReceiptStatusEnum {

    INSTOCK_WRK(1,1, "草稿",1, 2),
    INSTOCK_YRK(1,2, "完成",1, 2),
    OUTSTOCK_WCK(2,1, "草稿",1, 2),
    OUTSTOCK_YCK(2,2, "完成",1, 2);


    private Integer type;//单据类型
    private Integer code;//单据状态
    private String desc;

    private Integer lastCode;
    private Integer nextCode;

    private StockReceiptStatusEnum(Integer type, Integer code, String desc, Integer lastCode, Integer nextCode) {
        this.type = type;
        this.code = code;
        this.desc = desc;
        this.lastCode = lastCode;
        this.nextCode = nextCode;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getLastCode() {
        return lastCode;
    }

    public void setLastCode(Integer lastCode) {
        this.lastCode = lastCode;
    }

    public Integer getNextCode() {
        return nextCode;
    }

    public void setNextCode(Integer nextCode) {
        this.nextCode = nextCode;
    }

    public static String getDescByTypeAndCode(Integer type, Integer code) {
        String ret = null;
        for (StockReceiptStatusEnum enumItem : StockReceiptStatusEnum.values()) {
            if (enumItem.getCode().equals(code) && enumItem.getType().equals(type)) {
                ret = enumItem.getDesc();
            }
        }
        return ret;
    }

    public static Integer getNextCodeByTypeAndCode(Integer type, Integer code) {
        Integer ret = null;
        for (StockReceiptStatusEnum enumItem : StockReceiptStatusEnum.values()) {
            if (enumItem.getCode().equals(code) && enumItem.getType().equals(type)) {
                ret = enumItem.getNextCode();
            }
        }
        return ret;
    }
}
