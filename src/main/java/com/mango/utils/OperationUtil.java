package com.mango.utils;
import com.mango.constant.Constants;
import com.mango.dto.QueryCondition;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.Map;

public class OperationUtil {

	public static int operation(int a, String operation, int b) {
		if(operation.equals(Constants.ADD)) return a + b;
		if(operation.equals(Constants.REDUCE)) return a - b;

		return 0;
	}

	public static BigDecimal operation(BigDecimal a, String operation, BigDecimal b) {
		if(operation.equals(Constants.ADD)) return a.add(b);
		if(operation.equals(Constants.REDUCE)) return a.subtract(b);

		return BigDecimal.ZERO;
	}
	
}
