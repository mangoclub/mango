package com.mango.utils;
import com.mango.context.ContextContainer;
import com.mango.context.UserContext;
import com.mango.dto.StockReceiptDto;
import com.mango.framework.Beans;
import com.mango.service.NumberSequenceService;


public class SequenceUtil {

	public static String generateSerial(StockReceiptDto dto) {
		UserContext userContext = ContextContainer.getUserContext();
		String serial = Beans.getBean(NumberSequenceService.class).getNumberSequenceByShopIdAndType(userContext.getShopId(),dto.getReceiptType(), dto.getTypeName());
		return serial;
	}

}
