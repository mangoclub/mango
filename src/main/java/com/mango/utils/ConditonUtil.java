package com.mango.utils;
import com.mango.dto.QueryCondition;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public class ConditonUtil {

	public static QueryCondition generateCondition(HttpServletRequest httpServletRequest) {
		Map<String, String[]> map = httpServletRequest.getParameterMap();

		QueryCondition queryCondition = QueryCondition.builder()
				.supplierId(map.get("supplierId") == null ? null : Integer.parseInt(map.get("supplierId")[0]))
				.clientId(map.get("clientId") == null ? null : Integer.parseInt(map.get("clientId")[0]))
				.type(map.get("type") == null ? null : map.get("type")[0])
				.receiptId(map.get("receiptId") == null ? null : Integer.parseInt(map.get("receiptId")[0]))
				.current(map.get("current") == null ? null : map.get("current")[0])
				.pageSize(map.get("pageSize") == null ? null : map.get("pageSize")[0])
				.keyword(map.get("keyword") == null ? null : map.get("keyword")[0]).build();

		return queryCondition;
	}
	
}
