package com.mango.utils;
import java.text.NumberFormat;

public class NumberUtil {
	/**
	 * 补0
	 * @param target 要补0的数值
	 * @param number 最小整数位数  
	 * @return
	 */
	public static String format(long target,int number){
		// 得到一个NumberFormat的实例  
        NumberFormat nf = NumberFormat.getInstance();  
        // 设置是否使用分组  
        nf.setGroupingUsed(false);  
        // 设置最大整数位数  
        nf.setMaximumIntegerDigits(number);  
        // 设置最小整数位数  
        nf.setMinimumIntegerDigits(number);
		return nf.format(target);
	}
	
}
