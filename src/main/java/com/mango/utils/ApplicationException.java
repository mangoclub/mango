package com.mango.utils;

/**
 * Created by zhangxinyu on 16/7/12.
 */
public class ApplicationException extends RuntimeException {
    public ApplicationException() {
    }



    public ApplicationException(String message) {
        super(message);
    }


    public ApplicationException(String message, Object...args) {
        super(String.format(message,args));
    }

    public ApplicationException(String message, Throwable cause) {
        super(message, cause);
    }
}
