package com.mango.dao;


import com.mango.entity.Pay;

public interface PayDao extends BasicDao<Pay>{

    void changeStatus(Pay pay);
}
