package com.mango.dao;


import com.mango.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserDao extends BasicDao<User>{

    List<User> findByAccountName(@Param("accountName") String accountName);

    List<User> findByShopId(@Param("shopId") Integer shopId);

    void changePassword(User entity);

    void updateLoginTime(User entity);

}
