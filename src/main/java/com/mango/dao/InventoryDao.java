package com.mango.dao;


import com.mango.entity.Inventory;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface InventoryDao extends BasicDao<Inventory>{

    List<Inventory> queryInventory(@Param("shopId") Integer shopId, @Param("productIds") List<Integer> productIds);

    Inventory queryInventoryByProductId(@Param("shopId") Integer shopId, @Param("productId") Integer productId);

}
