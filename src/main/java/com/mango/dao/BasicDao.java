package com.mango.dao;

import com.mango.dto.QueryCondition;
import com.mango.entity.BasicEntity;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component("commonBasicDao")
public interface BasicDao<T extends BasicEntity> {
    T findById(Integer id);

    int create(T entity);

    void update(T entity);

    void delete(T entity);

    void batchDeleteByIds(@Param("ids") List<Integer> ids, @Param("updateTime") Date updateTime);

    void batchCreate(@Param("list") List<T> list);

    void batchDelete(@Param("list") List<T> list);

    List<T> queryByConditionWithAnd(T t);

    List<T> queryByCondition(@Param("condition") QueryCondition condition);

    int queryCountByCondition(@Param("condition") QueryCondition condition);

    List<T> queryList(@Param("condition") QueryCondition condition);
}