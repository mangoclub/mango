package com.mango.dao;

import com.mango.entity.DistributedLock;
import org.apache.ibatis.annotations.Param;

public interface DistributedLockDao extends BasicDao<DistributedLock> {

    DistributedLock findByKeyAndType(@Param("key") String key,
                                     @Param("type") Integer type);

    void remove(DistributedLock distributedLock);

}