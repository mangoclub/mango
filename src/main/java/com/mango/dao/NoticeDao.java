package com.mango.dao;


import com.mango.entity.Notice;

public interface NoticeDao extends BasicDao<Notice>{


    void publish(Notice entity);

    void release(Notice entity);

    Notice findByStatus(Integer status);


}
