package com.mango.dao;


import com.mango.entity.Shop;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ShopDao extends BasicDao<Shop>{

    List<Shop> findByAccountName(@Param("accountName") String accountName);

    void changePassword(Shop shop);

    boolean extend(Shop shop);

}
