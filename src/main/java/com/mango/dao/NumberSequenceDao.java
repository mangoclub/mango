package com.mango.dao;

import com.mango.entity.NumberSequence;
import org.apache.ibatis.annotations.Param;


public interface NumberSequenceDao extends BasicDao<NumberSequence> {

    NumberSequence getNumberByShopIdAndType(@Param("shopId") int shopId,
                                            @Param("type") int type);

}