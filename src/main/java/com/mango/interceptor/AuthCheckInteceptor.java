package com.mango.interceptor;

import com.mango.annotation.AuthCheckAnnotation;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class AuthCheckInteceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        HandlerMethod methodHandler=(HandlerMethod) handler;
        AuthCheckAnnotation auth=methodHandler.getMethodAnnotation(AuthCheckAnnotation.class);
        //如果方法中添加了@AuthCheckAnnotation 这里的auth不为null
        //如果@AuthCheckAnnotation(check=false) 这里的auth为false,即不用进行拦截验证，@AuthCheckAnnotation默认为前面定义的true　　
        if(auth==null&&!auth.check()){
            return false;
        }
        return true;
    }

}
