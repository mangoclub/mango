package com.mango.interceptor;

import com.google.common.base.Strings;
import com.mango.context.ContextContainer;
import com.mango.context.ContextHandler;
import com.mango.dto.ShopDto;
import com.mango.utils.ApplicationException;
import com.mango.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.security.auth.login.LoginException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class UserContextInterceptor implements HandlerInterceptor {

    @Autowired
    ContextHandler contextHandler;

    @Override
    public boolean preHandle( HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) throws Exception {

        checkOperationLogin(httpServletRequest);
        String userInfo = (String) httpServletRequest.getSession().getAttribute("userInfo");
        if(!StringUtils.isEmpty(userInfo)){
            contextHandler.enrichUserInfo(userInfo);
            //layout.ftl 中展示用户姓名
            httpServletRequest.setAttribute("userContext", ContextContainer.getUserContext());
            httpServletRequest.getSession().setAttribute("userContext", ContextContainer.getUserContext());
        } else{
            throw new LoginException("您已处于登出状态，请重新登陆！");
        }

        if(ContextContainer.getUserContext() != null) {
            ShopDto shopDto = ContextContainer.getUserContext().getShopDto();
            if(!Strings.isNullOrEmpty(shopDto.getExtend3()) && DateUtils.getDateFormat(shopDto.getExtend3()).compareTo(new Date()) <= 0
                    && httpServletRequest.getMethod().equals("POST") && !httpServletRequest.getServletPath().contains("/api/pay/")) {
                throw new ApplicationException("您的帐号使用已到期！");
            }
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {
        ContextContainer.clear();
    }

    private Map<String,String> readCookieMap(HttpServletRequest request){
        Map<String,String> cookieMap = new HashMap<String,String>();
        Cookie[] cookies = request.getCookies();
        if(null!=cookies){
            for(Cookie cookie : cookies){
                cookieMap.put(cookie.getName(), cookie.getValue());
            }
        }
        return cookieMap;
    }

    private void checkOperationLogin(HttpServletRequest httpServletRequest){

    }
}
