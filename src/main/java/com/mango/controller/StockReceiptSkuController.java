package com.mango.controller;

import com.mango.dto.StockReceiptSkuDto;
import com.mango.dto.QueryCondition;
import com.mango.dto.ResultMap;
import com.mango.service.StockReceiptSkuService;
import com.mango.utils.ConditonUtil;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/api/stockReceiptSku")
@Log4j
public class StockReceiptSkuController extends AjaxBase{

    @Autowired
    private StockReceiptSkuService service;


    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @ResponseBody
    public ResultMap create(@RequestBody StockReceiptSkuDto dto, HttpServletRequest httpServletRequest) throws Exception{

        try {
            dto.setIp(httpServletRequest.getRemoteAddr());
            return generateResultMap(SUCCESS_CODE, ADD_SUCCESS, service.create(dto));
        } catch (Exception e) {
            return generateResultMap(e);
        }
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public ResultMap update(@RequestBody StockReceiptSkuDto dto, HttpServletRequest httpServletRequest) throws Exception{

        try {
            dto.setIp(httpServletRequest.getRemoteAddr());
            return generateResultMap(SUCCESS_CODE, SAVE_SUCCESS, service.update(dto));
        } catch (Exception e) {
            return generateResultMap(e);
        }
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    @ResponseBody
    public ResultMap delete(@RequestBody List<Integer> ids, HttpServletRequest httpServletRequest) throws Exception{

        try {
            return generateResultMap(SUCCESS_CODE, SAVE_SUCCESS, service.delete(ids));
        } catch (Exception e) {
            return generateResultMap(e);
        }
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    @ResponseBody
    public ResultMap all(HttpServletRequest httpServletRequest) throws Exception{

        try {
            QueryCondition queryCondition = ConditonUtil.generateCondition(httpServletRequest);

            return generateResultMap(SUCCESS_CODE, QUERY_SUCCESS, service.query(queryCondition));
        } catch (Exception e) {
            return generateResultMap(e);
        }
    }

}