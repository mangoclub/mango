package com.mango.controller;

import com.mango.constant.Constants;
import com.mango.context.UserContext;
import com.mango.dto.QueryCondition;
import com.mango.dto.ResultMap;
import com.mango.dto.ShopDto;
import com.mango.service.ShopService;
import com.mango.utils.ConditonUtil;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/api/shop")
@Log4j
public class ShopController extends AjaxBase{

    @Autowired
    private ShopService service;


    @RequestMapping(value = "/register", method = RequestMethod.POST)
    @ResponseBody
    public ResultMap register(@RequestBody ShopDto shopDto, HttpServletRequest httpServletRequest) throws Exception{

        try {
            shopDto.setIp(httpServletRequest.getRemoteAddr());
            return generateResultMap(SUCCESS_CODE, REGISTER_SUCCESS, service.create(shopDto));
        } catch (Exception e) {
            return generateResultMap(e);
        }
    }

    /**
     * 用户登陆
     * 不回被拦截器拦截,userContext 在本方法中复制
     * @param httpSession
     * @return
     * @throws Exception
     */
//    @RequestMapping(value = "/login", method = RequestMethod.POST)
//    @ResponseBody
//    public ResultMap login(@RequestBody ShopDto shopDto, HttpSession httpSession, UserContext userContext) throws Exception {
//
//        try {
//            return generateResultMap(SUCCESS_CODE, LOGIN_SUCCESS, service.login(shopDto, httpSession, userContext));
//        } catch (Exception e) {
//            return generateResultMap(e);
//        }
//
//    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    @ResponseBody
    public ResultMap logout(HttpSession httpSession) throws Exception {
        try {
            return generateResultMap(SUCCESS_CODE, LOGOUT_SUCCESS, service.logout(httpSession));
        } catch (Exception e) {
            return generateResultMap(e);
        }

    }

//    @RequestMapping(value = "/changePassword", method = RequestMethod.POST)
//    @ResponseBody
//    public ResultMap changePassword(@RequestBody ShopDto shopDto, HttpSession httpSession) throws Exception {
//
//        try {
//            return generateResultMap(SUCCESS_CODE, UPDATE_SUCCESS, service.changePassword(shopDto, httpSession));
//        } catch (Exception e) {
//            return generateResultMap(e);
//        }
//
//    }

    @RequestMapping(value = "/userInfo", method = RequestMethod.GET)
    @ResponseBody
    public ResultMap userInfo(HttpSession httpSession) throws Exception {

        try {
            return generateResultMap(SUCCESS_CODE, QUERY_SUCCESS, service.userInfo(httpSession));
        } catch (Exception e) {
            return generateResultMap(e);
        }

    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    @ResponseBody
    public ResultMap all(HttpServletRequest httpServletRequest) throws Exception{

        try {

            QueryCondition queryCondition = ConditonUtil.generateCondition(httpServletRequest);

            return generateResultMap(SUCCESS_CODE, QUERY_SUCCESS, service.query(queryCondition));
        } catch (Exception e) {
            return generateResultMap(e);
        }
    }

//    @RequestMapping(value = "/resetPassword", method = RequestMethod.POST)
//    @ResponseBody
//    public ResultMap resetPassword(@RequestBody ShopDto shopDto, HttpSession httpSession) throws Exception {
//
//        try {
//            shopDto.setNewPassword(Constants.DEFAULT_PASSWORD);
//            return generateResultMap(SUCCESS_CODE, UPDATE_SUCCESS, service.resetPassword(shopDto, httpSession));
//        } catch (Exception e) {
//            return generateResultMap(e);
//        }
//
//    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public ResultMap update(@RequestBody ShopDto dto, HttpServletRequest httpServletRequest) throws Exception {

        try {
            dto.setIp(httpServletRequest.getRemoteAddr());
            return generateResultMap(SUCCESS_CODE, UPDATE_SUCCESS, service.update(dto));
        } catch (Exception e) {
            return generateResultMap(e);
        }

    }
}