package com.mango.controller;

import com.mango.dto.*;
import com.mango.service.VendorService;
import com.mango.utils.ConditonUtil;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/api/vendor")
@Log4j
public class VendorController extends AjaxBase{

    @Autowired
    private VendorService vendorService;


    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @ResponseBody
    public ResultMap create(@RequestBody VendorDto vendorDto, HttpServletRequest httpServletRequest) throws Exception{

        try {
            vendorDto.setIp(httpServletRequest.getRemoteAddr());
            return generateResultMap(SUCCESS_CODE, CREATE_SUCCESS, vendorService.create(vendorDto));
        } catch (Exception e) {
            return generateResultMap(e);
        }
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public ResultMap update(@RequestBody VendorDto vendorDto, HttpServletRequest httpServletRequest) throws Exception{

        try {
            vendorDto.setIp(httpServletRequest.getRemoteAddr());
            return generateResultMap(SUCCESS_CODE, SAVE_SUCCESS, vendorService.update(vendorDto));
        } catch (Exception e) {
            return generateResultMap(e);
        }
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    @ResponseBody
    public ResultMap all(HttpServletRequest httpServletRequest) throws Exception{

        try {

            return generateResultMap(SUCCESS_CODE, QUERY_SUCCESS, vendorService.query(ConditonUtil.generateCondition(httpServletRequest)));
        } catch (Exception e) {
            return generateResultMap(e);
        }
    }

    @RequestMapping(value = "/get", method = RequestMethod.GET)
    @ResponseBody
    public ResultMap get(HttpServletRequest httpServletRequest) throws Exception{

        try {

            VendorWebDto webDto = vendorService.query(ConditonUtil.generateCondition(httpServletRequest));
            List<VendorDto> list = webDto.getList();

            return generateResultMap(SUCCESS_CODE, QUERY_SUCCESS, vendorService.generateLabelValueList(list));
        } catch (Exception e) {
            return generateResultMap(e);
        }
    }

}