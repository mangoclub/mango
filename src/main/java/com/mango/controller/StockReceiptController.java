package com.mango.controller;

import com.mango.dto.InventoryChangeDto;
import com.mango.dto.StockReceiptDto;
import com.mango.dto.QueryCondition;
import com.mango.dto.ResultMap;
import com.mango.enums.NumberSequenceTypeEnum;
import com.mango.enums.StockReceiptStatusEnum;
import com.mango.service.StockReceiptService;
import com.mango.utils.Assert;
import com.mango.utils.ConditonUtil;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Controller
@RequestMapping("/api/stockReceipt")
@Log4j
public class StockReceiptController extends AjaxBase{

    @Autowired
    private StockReceiptService service;

    /**
     * 以下为入库
     */
    @RequestMapping(value = "/inStock/create", method = RequestMethod.POST)
    @ResponseBody
    public ResultMap createInstock(HttpServletRequest httpServletRequest) throws Exception{

        try {
            StockReceiptDto dto = new StockReceiptDto();
            dto.setReceiptType(NumberSequenceTypeEnum.RKD.getCode());
            dto.setTypeName(NumberSequenceTypeEnum.RKD.getName());
            dto.setStatus(StockReceiptStatusEnum.INSTOCK_WRK.getCode());
            dto.setIp(httpServletRequest.getRemoteAddr());

            return generateResultMap(SUCCESS_CODE, CREATE_SUCCESS, service.findById(service.create(dto)));
        } catch (Exception e) {
            return generateResultMap(e);
        }
    }

    @RequestMapping(value = "/inStock/save", method = RequestMethod.POST)
    @ResponseBody
    public ResultMap updateInstock(@RequestBody StockReceiptDto dto, HttpServletRequest httpServletRequest) throws Exception{

        try {
            dto.setIp(httpServletRequest.getRemoteAddr());
            return generateResultMap(SUCCESS_CODE, SAVE_SUCCESS, service.update(dto));
        } catch (Exception e) {
            return generateResultMap(e);
        }
    }

    @RequestMapping(value = "/inStock/all", method = RequestMethod.GET)
    @ResponseBody
    public ResultMap allInstock(HttpServletRequest httpServletRequest) throws Exception{

        try {

            QueryCondition queryCondition = ConditonUtil.generateCondition(httpServletRequest);

            queryCondition.setReceiptType(NumberSequenceTypeEnum.RKD.getCode());

            return generateResultMap(SUCCESS_CODE, QUERY_SUCCESS, service.query(queryCondition));
        } catch (Exception e) {
            return generateResultMap(e);
        }
    }

    @RequestMapping(value = "/inStock/one", method = RequestMethod.GET)
    @ResponseBody
    public ResultMap oneInstock(HttpServletRequest httpServletRequest) throws Exception{

        try {
            Map<String, String[]> map = httpServletRequest.getParameterMap();

            Integer id = Integer.parseInt(map.get("id") == null ? "0" : map.get("id")[0]);

            return generateResultMap(SUCCESS_CODE, QUERY_SUCCESS, service.findById(id));
        } catch (Exception e) {
            return generateResultMap(e);
        }
    }

    @RequestMapping(value = "/inStock/push", method = RequestMethod.POST)
    @ResponseBody
    public ResultMap pushInstock(@RequestBody StockReceiptDto dto, HttpServletRequest httpServletRequest) throws Exception{

        try {

            return generateResultMap(SUCCESS_CODE, INSTOCK_SUCCESS, inventoryChange(dto));
        } catch (Exception e) {
            return generateResultMap(e);
        }
    }

    /**
     * 以上为入库，以下为出库***********************************************************************
     */
    @RequestMapping(value = "/outStock/create", method = RequestMethod.POST)
    @ResponseBody
    public ResultMap createOutstock(HttpServletRequest httpServletRequest) throws Exception{

        try {
            StockReceiptDto dto = new StockReceiptDto();
            dto.setReceiptType(NumberSequenceTypeEnum.CKD.getCode());
            dto.setTypeName(NumberSequenceTypeEnum.CKD.getName());
            dto.setStatus(StockReceiptStatusEnum.OUTSTOCK_WCK.getCode());
            dto.setIp(httpServletRequest.getRemoteAddr());

            return generateResultMap(SUCCESS_CODE, CREATE_SUCCESS, service.findById(service.create(dto)));
        } catch (Exception e) {
            return generateResultMap(e);
        }
    }

    @RequestMapping(value = "/outStock/save", method = RequestMethod.POST)
    @ResponseBody
    public ResultMap updateOutstock(@RequestBody StockReceiptDto dto, HttpServletRequest httpServletRequest) throws Exception{

        try {
            dto.setIp(httpServletRequest.getRemoteAddr());
            return generateResultMap(SUCCESS_CODE, SAVE_SUCCESS, service.update(dto));
        } catch (Exception e) {
            return generateResultMap(e);
        }
    }

    @RequestMapping(value = "/outStock/all", method = RequestMethod.GET)
    @ResponseBody
    public ResultMap allOutstock(HttpServletRequest httpServletRequest) throws Exception{

        try {

            QueryCondition queryCondition = ConditonUtil.generateCondition(httpServletRequest);

            queryCondition.setReceiptType(NumberSequenceTypeEnum.CKD.getCode());

            return generateResultMap(SUCCESS_CODE, QUERY_SUCCESS, service.query(queryCondition));
        } catch (Exception e) {
            return generateResultMap(e);
        }
    }

    @RequestMapping(value = "/outStock/one", method = RequestMethod.GET)
    @ResponseBody
    public ResultMap oneOutstock(HttpServletRequest httpServletRequest) throws Exception{

        try {
            Map<String, String[]> map = httpServletRequest.getParameterMap();

            Integer id = Integer.parseInt(map.get("id") == null ? "0" : map.get("id")[0]);

            return generateResultMap(SUCCESS_CODE, QUERY_SUCCESS, service.findById(id));
        } catch (Exception e) {
            return generateResultMap(e);
        }
    }

    @RequestMapping(value = "/outStock/push", method = RequestMethod.POST)
    @ResponseBody
    public ResultMap pushOutstock(@RequestBody StockReceiptDto dto, HttpServletRequest httpServletRequest) throws Exception{

        try {

            return generateResultMap(SUCCESS_CODE, OUTSTOCK_SUCCESS, inventoryChange(dto));
        } catch (Exception e) {
            return generateResultMap(e);
        }
    }


    //私有方法
    private boolean inventoryChange(StockReceiptDto dto) {

        InventoryChangeDto changeDto = InventoryChangeDto.builder().build();
        dto = service.findById(dto.getId());
        changeDto.setShopId(dto.getShopId());
        changeDto.setType(dto.getReceiptType());
        changeDto.setReceiptId(dto.getId());
        changeDto.setSkuDtoList(dto.getList());

        Assert.assertFalse(!dto.getStatus().equals(1), "您的单据状态不能出入库！");

        service.inventoryChange(changeDto);
        dto.setStatus(StockReceiptStatusEnum.getNextCodeByTypeAndCode(dto.getReceiptType(), dto.getStatus()));
        service.update(dto);

        return true;
    }

}