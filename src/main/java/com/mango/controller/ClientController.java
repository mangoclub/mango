package com.mango.controller;

import com.mango.dto.ClientDto;
import com.mango.dto.ClientWebDto;
import com.mango.dto.QueryCondition;
import com.mango.dto.ResultMap;
import com.mango.service.ClientService;
import com.mango.utils.ConditonUtil;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/api/client")
@Log4j
public class ClientController extends AjaxBase{

    @Autowired
    private ClientService clientService;


    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @ResponseBody
    public ResultMap create(@RequestBody ClientDto clientDto, HttpServletRequest httpServletRequest) throws Exception{

        try {
            clientDto.checkAndTransfer(clientDto);
            clientDto.setIp(httpServletRequest.getRemoteAddr());
            return generateResultMap(SUCCESS_CODE, CREATE_SUCCESS, clientService.create(clientDto));
        } catch (Exception e) {
            return generateResultMap(e);
        }
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public ResultMap update(@RequestBody ClientDto clientDto, HttpServletRequest httpServletRequest) throws Exception{

        try {
            clientDto.setIp(httpServletRequest.getRemoteAddr());
            return generateResultMap(SUCCESS_CODE, SAVE_SUCCESS, clientService.update(clientDto));
        } catch (Exception e) {
            return generateResultMap(e);
        }
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    @ResponseBody
    public ResultMap all(HttpServletRequest httpServletRequest) throws Exception{

        try {

            QueryCondition queryCondition = ConditonUtil.generateCondition(httpServletRequest);

            return generateResultMap(SUCCESS_CODE, QUERY_SUCCESS, clientService.query(queryCondition));
        } catch (Exception e) {
            return generateResultMap(e);
        }
    }

    @RequestMapping(value = "/get", method = RequestMethod.GET)
    @ResponseBody
    public ResultMap get(HttpServletRequest httpServletRequest) throws Exception{

        try {

            ClientWebDto webDto = clientService.query(ConditonUtil.generateCondition(httpServletRequest));
            List<ClientDto> list = webDto.getList();

            return generateResultMap(SUCCESS_CODE, QUERY_SUCCESS, clientService.generateLabelValueList(list));
        } catch (Exception e) {
            return generateResultMap(e);
        }
    }

}