package com.mango.controller;

import com.aliyun.oss.OSSClient;
import com.mango.dto.ProductDto;
import com.mango.dto.ResultMap;
import com.mango.service.ClientService;
import com.mango.utils.AliyunOSSClientUtil;
import lombok.extern.log4j.Log4j;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.Date;

import com.mango.constant.OSSClientConstants;

import static com.mango.constant.OSSClientConstants.BACKET_NAME;
import static com.mango.constant.OSSClientConstants.ENDPOINT;
import static com.mango.constant.OSSClientConstants.FOLDER;


@Controller
@RequestMapping("/api/pic")
@Log4j
public class PicController extends AjaxBase{

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ResponseBody
    public ResultMap save(@RequestParam("file") MultipartFile file, HttpServletRequest request) throws Exception{

        try {
            OSSClient ossClient= AliyunOSSClientUtil.getOSSClient();
            //上传文件
            long now = new Date().getTime();
            String filePath = request.getSession().getServletContext().getRealPath("/") + now + file.getOriginalFilename();
            // 转存文件
            file.transferTo(new File(filePath));
            File fl = new File(filePath);
            String md5key = AliyunOSSClientUtil.uploadObject2OSS(ossClient, fl, BACKET_NAME, FOLDER);
            //上传后的文件MD5数字唯一签名:40F4131427068E08451D37F02021473A
            if (fl.exists()) fl.delete();
            return generateResultMap(SUCCESS_CODE, SAVE_SUCCESS , ProductDto.builder()
                    .imageUrl(String.format("http://%s.%s/%s%s", BACKET_NAME, ENDPOINT, FOLDER, now + file.getOriginalFilename())));
        } catch (Exception e) {
            return generateResultMap(e);
        }
    }

}