package com.mango.controller;

import lombok.extern.log4j.Log4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.naming.NoPermissionException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@Controller
@Log4j
public class IndexController {

    @RequestMapping(value = "/**")
    public String index(ModelMap model, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws NoPermissionException {
        String path = httpServletRequest.getServletPath();
        if(path.equals("/") || path.equals("") || path.equals("/register") || path.equals("/login")) {
            return "home";
        }else if(path.equals("/backstage/shop") || path.equals("/backstage/notice") || path.equals("/backstage/log") || path.equals("/backstage/paid")){
            return "backstage";
        }else {
            return "app";
        }
    }


}
