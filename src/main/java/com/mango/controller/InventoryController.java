package com.mango.controller;

import com.mango.dto.*;
import com.mango.service.InventoryService;
import com.mango.utils.ConditonUtil;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/api/inventory")
@Log4j
public class InventoryController extends AjaxBase{

    @Autowired
    private InventoryService service;



    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ResponseBody
    public ResultMap updateInstock(@RequestBody InventoryDto dto, HttpServletRequest httpServletRequest) throws Exception{

        try {
            dto.setIp(httpServletRequest.getRemoteAddr());
            return generateResultMap(SUCCESS_CODE, SAVE_SUCCESS, service.update(dto));
        } catch (Exception e) {
            return generateResultMap(e);
        }
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    @ResponseBody
    public ResultMap all(HttpServletRequest httpServletRequest) throws Exception{

        try {

            QueryCondition queryCondition = ConditonUtil.generateCondition(httpServletRequest);

            return generateResultMap(SUCCESS_CODE, QUERY_SUCCESS, service.query(queryCondition));
        } catch (Exception e) {
            return generateResultMap(e);
        }
    }




}