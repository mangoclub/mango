package com.mango.controller;

import com.mango.dto.ProductDto;
import com.mango.dto.ProductWebDto;
import com.mango.dto.QueryCondition;
import com.mango.dto.ResultMap;
import com.mango.service.ProductService;
import com.mango.utils.ConditonUtil;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/api/product")
@Log4j
public class ProductController extends AjaxBase{

    @Autowired
    private ProductService service;


    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @ResponseBody
    public ResultMap create(@RequestBody ProductDto dto, HttpServletRequest httpServletRequest) throws Exception{

        try {
            dto.setIp(httpServletRequest.getRemoteAddr());
            return generateResultMap(SUCCESS_CODE, CREATE_SUCCESS, service.create(dto));
        } catch (Exception e) {
            return generateResultMap(e);
        }
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public ResultMap update(@RequestBody ProductDto dto, HttpServletRequest httpServletRequest) throws Exception{

        try {
            dto.setIp(httpServletRequest.getRemoteAddr());
            return generateResultMap(SUCCESS_CODE, SAVE_SUCCESS, service.update(dto));
        } catch (Exception e) {
            return generateResultMap(e);
        }
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    @ResponseBody
    public ResultMap all(HttpServletRequest httpServletRequest) throws Exception{

        try {

            QueryCondition queryCondition = ConditonUtil.generateCondition(httpServletRequest);

            return generateResultMap(SUCCESS_CODE, QUERY_SUCCESS, service.query(queryCondition));
        } catch (Exception e) {
            return generateResultMap(e);
        }
    }

    @RequestMapping(value = "/get", method = RequestMethod.GET)
    @ResponseBody
    public ResultMap get(HttpServletRequest httpServletRequest) throws Exception{

        try {

            ProductWebDto webDto = service.query(ConditonUtil.generateCondition(httpServletRequest));
            List<ProductDto> list = webDto.getList();

            return generateResultMap(SUCCESS_CODE, QUERY_SUCCESS, service.generateLabelValueList(list));
        } catch (Exception e) {
            return generateResultMap(e);
        }
    }

    @RequestMapping(value = "/find", method = RequestMethod.GET)
    @ResponseBody
    public ResultMap find(HttpServletRequest httpServletRequest) throws Exception{

        try {
            Map<String, String[]> map = httpServletRequest.getParameterMap();

            Integer id = Integer.parseInt(map.get("id")[0]);
            return generateResultMap(SUCCESS_CODE, QUERY_SUCCESS, service.findById(id));
        } catch (Exception e) {
            return generateResultMap(e);
        }
    }
}