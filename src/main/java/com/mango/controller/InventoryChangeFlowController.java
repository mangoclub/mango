package com.mango.controller;

import com.mango.dto.QueryCondition;
import com.mango.dto.ResultMap;
import com.mango.enums.NumberSequenceTypeEnum;
import com.mango.service.InventoryChangeFlowService;
import com.mango.utils.ConditonUtil;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/api/inventoryChangeFlow")
@Log4j
public class InventoryChangeFlowController extends AjaxBase{

    @Autowired
    private InventoryChangeFlowService service;



    @RequestMapping(value = "/all", method = RequestMethod.GET)
    @ResponseBody
    public ResultMap all(HttpServletRequest httpServletRequest) throws Exception{

        try {

            QueryCondition queryCondition = ConditonUtil.generateCondition(httpServletRequest);

            return generateResultMap(SUCCESS_CODE, QUERY_SUCCESS, service.query(queryCondition));
        } catch (Exception e) {
            return generateResultMap(e);
        }
    }

    @RequestMapping(value = "/in", method = RequestMethod.GET)
    @ResponseBody
    public ResultMap in(HttpServletRequest httpServletRequest) throws Exception{

        try {

            QueryCondition queryCondition = QueryCondition.builder().receiptType(NumberSequenceTypeEnum.RKD.getCode()).build();

            return generateResultMap(SUCCESS_CODE, QUERY_SUCCESS, service.queryCollect(queryCondition));
        } catch (Exception e) {
            return generateResultMap(e);
        }
    }

    @RequestMapping(value = "/out", method = RequestMethod.GET)
    @ResponseBody
    public ResultMap out(HttpServletRequest httpServletRequest) throws Exception{

        try {

            QueryCondition queryCondition = QueryCondition.builder().receiptType(NumberSequenceTypeEnum.CKD.getCode()).build();

            return generateResultMap(SUCCESS_CODE, QUERY_SUCCESS, service.queryCollect(queryCondition));
        } catch (Exception e) {
            return generateResultMap(e);
        }
    }

}