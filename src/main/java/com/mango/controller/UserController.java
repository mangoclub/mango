package com.mango.controller;

import com.mango.constant.Constants;
import com.mango.context.UserContext;
import com.mango.dto.QueryCondition;
import com.mango.dto.ResultMap;
import com.mango.dto.UserDto;
import com.mango.service.UserService;
import com.mango.utils.ConditonUtil;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/api/user")
@Log4j
public class UserController extends AjaxBase{

    @Autowired
    private UserService service;


    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @ResponseBody
    public ResultMap create(@RequestBody UserDto dto, HttpServletRequest httpServletRequest) throws Exception{

        try {
            dto.setIp(httpServletRequest.getRemoteAddr());
            return generateResultMap(SUCCESS_CODE, REGISTER_SUCCESS, service.create(dto));
        } catch (Exception e) {
            return generateResultMap(e);
        }
    }

    /**
     * 用户登陆
     * 不回被拦截器拦截,userContext 在本方法中复制
     * @param httpSession
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ResponseBody
    public ResultMap login(@RequestBody UserDto dto, HttpSession httpSession, UserContext userContext, HttpServletRequest httpServletRequest) throws Exception {

        try {
            return generateResultMap(SUCCESS_CODE, LOGIN_SUCCESS, service.login(dto, httpSession, userContext, httpServletRequest));
        } catch (Exception e) {
            return generateResultMap(e);
        }

    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    @ResponseBody
    public ResultMap logout(HttpSession httpSession) throws Exception {
        try {
            return generateResultMap(SUCCESS_CODE, LOGOUT_SUCCESS, service.logout(httpSession));
        } catch (Exception e) {
            return generateResultMap(e);
        }

    }

    @RequestMapping(value = "/changePassword", method = RequestMethod.POST)
    @ResponseBody
    public ResultMap changePassword(@RequestBody UserDto dto, HttpSession httpSession) throws Exception {

        try {
            return generateResultMap(SUCCESS_CODE, UPDATE_SUCCESS, service.changePassword(dto, httpSession));
        } catch (Exception e) {
            return generateResultMap(e);
        }

    }

    @RequestMapping(value = "/userInfo", method = RequestMethod.GET)
    @ResponseBody
    public ResultMap userInfo(HttpSession httpSession) throws Exception {

        try {
            return generateResultMap(SUCCESS_CODE, QUERY_SUCCESS, service.userInfo(httpSession));
        } catch (Exception e) {
            return generateResultMap(e);
        }

    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    @ResponseBody
    public ResultMap all(HttpServletRequest httpServletRequest) throws Exception{

        try {

            QueryCondition queryCondition = ConditonUtil.generateCondition(httpServletRequest);

            return generateResultMap(SUCCESS_CODE, QUERY_SUCCESS, service.query(queryCondition));
        } catch (Exception e) {
            return generateResultMap(e);
        }
    }

    @RequestMapping(value = "/resetPassword", method = RequestMethod.POST)
    @ResponseBody
    public ResultMap resetPassword(@RequestBody UserDto dto, HttpSession httpSession) throws Exception {

        try {
            dto.setNewPassword(Constants.DEFAULT_PASSWORD);
            return generateResultMap(SUCCESS_CODE, UPDATE_SUCCESS, service.resetPassword(dto, httpSession));
        } catch (Exception e) {
            return generateResultMap(e);
        }

    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public ResultMap update(@RequestBody UserDto dto, HttpServletRequest httpServletRequest) throws Exception {

        try {
            dto.setIp(httpServletRequest.getRemoteAddr());
            return generateResultMap(SUCCESS_CODE, UPDATE_SUCCESS, service.update(dto));
        } catch (Exception e) {
            return generateResultMap(e);
        }

    }
}